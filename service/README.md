# LUVSEATS INTERACTIVE CHARTS

## Description

Microservice used as backend to support interactive venue charts for the LuvSeats applications and ecosystem. Built with [NestJS](https://github.com/nestjs/nest) framework TypeScript.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Luvseat Development Team.

## Stay in touch

- Author - [Jorge Luna](https://ilusoft.ca)
- Website - [https://luvseats.com](https://luvseats.com/)

## License

TBD
