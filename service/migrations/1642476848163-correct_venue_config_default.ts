import { MigrationInterface, QueryRunner } from 'typeorm';

export class correctVenueConfigDefault1642476848163
  implements MigrationInterface {
  name = 'correctVenueConfigDefault1642476848163';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0"
        `);
    await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ALTER COLUMN "default_chart_id" DROP NOT NULL
        `);
    await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0" FOREIGN KEY ("default_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0"
        `);
    await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ALTER COLUMN "default_chart_id"
            SET NOT NULL
        `);
    await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0" FOREIGN KEY ("default_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
  }
}
