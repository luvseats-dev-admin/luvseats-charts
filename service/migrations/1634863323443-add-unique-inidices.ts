import {MigrationInterface, QueryRunner} from "typeorm";

export class addUniqueInidices1634863323443 implements MigrationInterface {
    name = 'addUniqueInidices1634863323443'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "UQ_1fa555240705e5ae381d93616bb" UNIQUE ("catalog_venue_id")
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "UQ_fdac615986aaf0401275492f60a" UNIQUE ("venue_name")
        `);
        await queryRunner.query(`
            ALTER TABLE "seat" DROP CONSTRAINT "FK_4a1fc126b2a9f395218254e975c"
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ALTER COLUMN "row_id"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "row" DROP CONSTRAINT "FK_26d930279bcbe25c687cbd541ae"
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ALTER COLUMN "section_id"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "section" DROP CONSTRAINT "FK_6469189fe679ac09426b67289a2"
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ALTER COLUMN "venue_chart_id"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart" DROP CONSTRAINT "FK_0d908fab8c69cf347c1a2708e90"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart"
            ADD CONSTRAINT "UQ_b110f7af7bdb556de37d2cebe44" UNIQUE ("chart_name")
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart"
            ALTER COLUMN "venue_id"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ADD CONSTRAINT "FK_4a1fc126b2a9f395218254e975c" FOREIGN KEY ("row_id") REFERENCES "row"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ADD CONSTRAINT "FK_26d930279bcbe25c687cbd541ae" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ADD CONSTRAINT "FK_6469189fe679ac09426b67289a2" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart"
            ADD CONSTRAINT "FK_0d908fab8c69cf347c1a2708e90" FOREIGN KEY ("venue_id") REFERENCES "venue_configuration"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "venue_chart" DROP CONSTRAINT "FK_0d908fab8c69cf347c1a2708e90"
        `);
        await queryRunner.query(`
            ALTER TABLE "section" DROP CONSTRAINT "FK_6469189fe679ac09426b67289a2"
        `);
        await queryRunner.query(`
            ALTER TABLE "row" DROP CONSTRAINT "FK_26d930279bcbe25c687cbd541ae"
        `);
        await queryRunner.query(`
            ALTER TABLE "seat" DROP CONSTRAINT "FK_4a1fc126b2a9f395218254e975c"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart"
            ALTER COLUMN "venue_id" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart" DROP CONSTRAINT "UQ_b110f7af7bdb556de37d2cebe44"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart"
            ADD CONSTRAINT "FK_0d908fab8c69cf347c1a2708e90" FOREIGN KEY ("venue_id") REFERENCES "venue_configuration"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ALTER COLUMN "venue_chart_id" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ADD CONSTRAINT "FK_6469189fe679ac09426b67289a2" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ALTER COLUMN "section_id" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ADD CONSTRAINT "FK_26d930279bcbe25c687cbd541ae" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ALTER COLUMN "row_id" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ADD CONSTRAINT "FK_4a1fc126b2a9f395218254e975c" FOREIGN KEY ("row_id") REFERENCES "row"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "UQ_fdac615986aaf0401275492f60a"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "UQ_1fa555240705e5ae381d93616bb"
        `);
    }

}
