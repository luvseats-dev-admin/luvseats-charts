import {MigrationInterface, QueryRunner} from "typeorm";

export class addCascadeDelete1641217724273 implements MigrationInterface {
    name = 'addCascadeDelete1641217724273'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "seat" DROP CONSTRAINT "FK_4a1fc126b2a9f395218254e975c"
        `);
        await queryRunner.query(`
            ALTER TABLE "row" DROP CONSTRAINT "FK_26d930279bcbe25c687cbd541ae"
        `);
        await queryRunner.query(`
            ALTER TABLE "section" DROP CONSTRAINT "FK_6469189fe679ac09426b67289a2"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ALTER COLUMN "default_chart_id"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "production_configuration" DROP CONSTRAINT "FK_f9e7d9c22df580e9c40bc3308f3"
        `);
        await queryRunner.query(`
            ALTER TABLE "production_configuration"
            ALTER COLUMN "venue_chart_id"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0" FOREIGN KEY ("default_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ADD CONSTRAINT "FK_4a1fc126b2a9f395218254e975c" FOREIGN KEY ("row_id") REFERENCES "row"("id") ON DELETE CASCADE ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ADD CONSTRAINT "FK_26d930279bcbe25c687cbd541ae" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE CASCADE ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ADD CONSTRAINT "FK_6469189fe679ac09426b67289a2" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE CASCADE ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "production_configuration"
            ADD CONSTRAINT "FK_f9e7d9c22df580e9c40bc3308f3" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "production_configuration" DROP CONSTRAINT "FK_f9e7d9c22df580e9c40bc3308f3"
        `);
        await queryRunner.query(`
            ALTER TABLE "section" DROP CONSTRAINT "FK_6469189fe679ac09426b67289a2"
        `);
        await queryRunner.query(`
            ALTER TABLE "row" DROP CONSTRAINT "FK_26d930279bcbe25c687cbd541ae"
        `);
        await queryRunner.query(`
            ALTER TABLE "seat" DROP CONSTRAINT "FK_4a1fc126b2a9f395218254e975c"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0"
        `);
        await queryRunner.query(`
            ALTER TABLE "production_configuration"
            ALTER COLUMN "venue_chart_id" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "production_configuration"
            ADD CONSTRAINT "FK_f9e7d9c22df580e9c40bc3308f3" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ALTER COLUMN "default_chart_id" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0" FOREIGN KEY ("default_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ADD CONSTRAINT "FK_6469189fe679ac09426b67289a2" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ADD CONSTRAINT "FK_26d930279bcbe25c687cbd541ae" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ADD CONSTRAINT "FK_4a1fc126b2a9f395218254e975c" FOREIGN KEY ("row_id") REFERENCES "row"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    }

}
