import {MigrationInterface, QueryRunner} from "typeorm";

export class initialSchema1633304302069 implements MigrationInterface {
    name = 'initialSchema1633304302069'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE "venue_configuration" (
                "id" SERIAL NOT NULL,
                "catalog_venue_id" integer,
                "venue_name" character varying NOT NULL,
                "default_chart_id" integer,
                CONSTRAINT "REL_fbcc6911a54b4f6390bb0a214a" UNIQUE ("default_chart_id"),
                CONSTRAINT "PK_851f9d2478a0937afb5aa5c9830" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "seat" (
                "id" SERIAL NOT NULL,
                "name" character varying NOT NULL,
                "map_seat_id" character varying,
                "chart_properties" jsonb,
                "row_id" integer,
                CONSTRAINT "PK_4e72ae40c3fbd7711ccb380ac17" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "row" (
                "id" SERIAL NOT NULL,
                "name" character varying NOT NULL,
                "map_row_id" character varying,
                "chart_properties" jsonb,
                "aliases" character varying array,
                "section_id" integer,
                CONSTRAINT "PK_8a1504a78acbc2e1273f69f03aa" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "section" (
                "id" SERIAL NOT NULL,
                "name" character varying NOT NULL,
                "map_section_id" character varying,
                "chart_properties" jsonb,
                "aliases" character varying array,
                "venue_chart_id" integer,
                CONSTRAINT "PK_3c41d2d699384cc5e8eac54777d" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "venue_chart" (
                "id" SERIAL NOT NULL,
                "chart_name" character varying NOT NULL,
                "background_image" character varying,
                "chart_properties" jsonb,
                "venue_id" integer,
                CONSTRAINT "PK_a8df6273655ea8f645000b35221" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "production_configuration" (
                "id" SERIAL NOT NULL,
                "production_name" character varying NOT NULL,
                "catalog_production_id" integer,
                "status" character varying NOT NULL DEFAULT 'INACTIVE',
                "venue_chart_id" integer,
                CONSTRAINT "PK_1d68af5154e730f6f43808f0987" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration"
            ADD CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0" FOREIGN KEY ("default_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "seat"
            ADD CONSTRAINT "FK_4a1fc126b2a9f395218254e975c" FOREIGN KEY ("row_id") REFERENCES "row"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "row"
            ADD CONSTRAINT "FK_26d930279bcbe25c687cbd541ae" FOREIGN KEY ("section_id") REFERENCES "section"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "section"
            ADD CONSTRAINT "FK_6469189fe679ac09426b67289a2" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart"
            ADD CONSTRAINT "FK_0d908fab8c69cf347c1a2708e90" FOREIGN KEY ("venue_id") REFERENCES "venue_configuration"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "production_configuration"
            ADD CONSTRAINT "FK_f9e7d9c22df580e9c40bc3308f3" FOREIGN KEY ("venue_chart_id") REFERENCES "venue_chart"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "production_configuration" DROP CONSTRAINT "FK_f9e7d9c22df580e9c40bc3308f3"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_chart" DROP CONSTRAINT "FK_0d908fab8c69cf347c1a2708e90"
        `);
        await queryRunner.query(`
            ALTER TABLE "section" DROP CONSTRAINT "FK_6469189fe679ac09426b67289a2"
        `);
        await queryRunner.query(`
            ALTER TABLE "row" DROP CONSTRAINT "FK_26d930279bcbe25c687cbd541ae"
        `);
        await queryRunner.query(`
            ALTER TABLE "seat" DROP CONSTRAINT "FK_4a1fc126b2a9f395218254e975c"
        `);
        await queryRunner.query(`
            ALTER TABLE "venue_configuration" DROP CONSTRAINT "FK_fbcc6911a54b4f6390bb0a214a0"
        `);
        await queryRunner.query(`
            DROP TABLE "production_configuration"
        `);
        await queryRunner.query(`
            DROP TABLE "venue_chart"
        `);
        await queryRunner.query(`
            DROP TABLE "section"
        `);
        await queryRunner.query(`
            DROP TABLE "row"
        `);
        await queryRunner.query(`
            DROP TABLE "seat"
        `);
        await queryRunner.query(`
            DROP TABLE "venue_configuration"
        `);
    }

}
