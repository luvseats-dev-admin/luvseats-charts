import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

export interface TokenPayload {
  userId: number;
  userLogin: string;
}

@Injectable()
export class AdminAuthService {
  constructor(private readonly jwtService: JwtService) {}

  public getJwtToken(userId: number, userLogin: string) {
    const payload: TokenPayload = { userId, userLogin };
    const token = this.jwtService.sign(payload);
    return token;
  }
}
