import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { AdminAuthService, TokenPayload } from './admin-auth.service';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';

@Injectable()
export class JwtAdminAuthStrategy extends PassportStrategy(Strategy) {
  private SYSUSERID: number = 999999;
  private SYSUSERLOGIN: string = 'SYSTEM';

  constructor(
    configService: ConfigService,
    private readonly authenticationService: AdminAuthService,
    private readonly logger: PinoLogger
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          if (request.headers['internal-adm-auth']) {
            const key = configService.get<string>('appConfig.internalServiceKey');
            if (!key) {
              this.logger.error({ msg: 'INTERNAL_SERVICE_KEY not configured' });
              throw new HttpException('System Configuration Issue', HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (request.headers['internal-adm-auth'] !== key) {
              return '';
            }
            const token = this.authenticationService.getJwtToken(this.SYSUSERID, this.SYSUSERLOGIN);
            return token;
          } else {
            return request?.headers['adm-auth-token'].toString();
          }
        },
      ]),
      secretOrKey: configService.get<string>('appConfig.secret'),
    });
  }

  async validate(payload: TokenPayload) {
    const user = {
      id: payload.userId,
      login: payload.userLogin,
      isActive: true,
    };
    return user;
  }
}
