import { Module } from '@nestjs/common';
import { AdminAuthService } from './admin-auth.service';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { JwtAdminAuthStrategy } from './jwt.admin-auth.strategy';

@Module({
  imports: [
    PassportModule,
    ConfigModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        secret: config.get<string>('appConfig.secret'),
        signOptions: {
          expiresIn: config.get<string>('appConfig.jwtExpire'),
        },
      }),
    }),
  ],
  providers: [AdminAuthService, JwtAdminAuthStrategy],
  controllers: [],
})
export class AdminAuthModule {}
