import { registerAs } from '@nestjs/config';

export default registerAs('appConfig', () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  logLevel: process.env.LOG_LEVEL || 'debug',
  environment: process.env.NODE_ENV,
  secret: process.env.APP_SECRET || 'dont_use_this_key',
  jwtExpire: process.env.JWT_EXPIRE_SPAN || '120m',
  corsAppServer: '*',
  internalServiceKey: process.env.INTERNAL_SERVICE_KEY,
  internalBaseUrl: process.env.INTERNAL_BASE_URL,
}));
