import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ProductionConfigurationModule } from './production-configuration/production-configuration.module';
import { VenueChartModule } from './venue-chart/venue-chart.module';
import { VenueConfigurationModule } from './venue-configuration/venue-configuration.module';
import appConfig from './config/app-config';
import dbConfig from './config/db-config';
import { UnitOfWorkModule } from './utils/dbutils/UnitOfWork.module';
import { AdminAuthModule } from './admin-auth/admin-auth.module';
import { GCPLogSeverity } from './utils/constants';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.local.env', '.env'],
      isGlobal: true,
      load: [appConfig, dbConfig],
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          pinoHttp: {
            serializers: {
              req(r) {
                r.body = r.raw.body;
                return r;
              },
            },
            level: config.get<string>('appConfig.logLevel'),
            formatters: {
              level: (level: string, value: number) => {
                return { level: value, severity: GCPLogSeverity[level] };
              },
            },
            prettyPrint:
              config.get<string>('appConfig.environment') !== 'development'
                ? false
                : {
                    colorize: true,
                    singleLine: true,
                    translateTime: true,
                  },
          },
        };
      },
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const dbcfg: TypeOrmModuleOptions = {
          type: 'postgres',
          host: config.get<string>('dbConfig.host'),
          port: config.get<number>('dbConfig.port'),
          username: config.get<string>('dbConfig.username'),
          password: config.get<string>('dbConfig.password'),
          database: config.get<string>('dbConfig.database'),
          schema: config.get<string>('dbConfig.schema'),
          logging: config.get<boolean>('dbConfig.logging'),
          extra: {
            socketPath: config.get<string>('dbConfig.socketPath'),
          },
          autoLoadEntities: true,
          synchronize: false,
        };
        return dbcfg;
      },
    }),
    UnitOfWorkModule,
    ProductionConfigurationModule,
    VenueChartModule,
    VenueConfigurationModule,
    AdminAuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
