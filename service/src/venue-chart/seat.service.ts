import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from 'luvseats-crud-typeorm';
import { Repository } from 'typeorm';
import { Seat } from './seat.entity';

@Injectable()
export class SeatService extends TypeOrmCrudService<Seat> {
  constructor(
    @InjectRepository(Seat)
    public repo: Repository<Seat>,
  ) {
    super(repo);
  }
}
