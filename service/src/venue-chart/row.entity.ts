import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { Seat } from './seat.entity';
import { Section } from './section.entity';

@Entity()
export class Row {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('character varying')
  name: string;

  @Column('character varying', { nullable: true })
  map_row_id: string;

  @Column('jsonb', { nullable: true })
  chart_properties: object;

  @Column('character varying', { array: true, nullable: true })
  aliases: string[];

  @ManyToOne(() => Section, (s) => s.rows, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'section_id' })
  section: Section;

  @Column()
  @RelationId((c: Row) => c.section)
  section_id: number;

  @OneToMany(() => Seat, (s) => s.row)
  seats: Seat[];
}
