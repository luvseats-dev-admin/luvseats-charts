import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { Crud, CrudController } from '@rewiko/crud';
import JwtAdminAuthGuard from '../admin-auth/jwt.admin-auth.guard';
import {
  CreateSectionDto,
  CreateVenueChartDto,
} from './dto/create-venue-chart.dto';
import { GetVenueChartDto } from './dto/get-venue-chart.dto';
import {
  VenueChartResponseDto,
  VenueChartSectionDto,
} from './dto/venue-chart-response.dto';
import { VenueChart } from './venue-chart.entity';
import { VenueChartService } from './venue-chart.service';

@Crud({
  model: {
    type: VenueChart,
  },
})
@Controller('venue-chart')
export class VenueChartController implements CrudController<VenueChart> {
  constructor(public service: VenueChartService) {}

  @Get('chart-properties')
  async GetVenueChartById(
    @Query(ValidationPipe)
    {
      id,
      catalogVenueId,
      catalogProductionId,
      addSectionDetails,
    }: GetVenueChartDto,
  ) {
    const chart = await this.service.GetVenueChartById(
      id,
      catalogVenueId,
      catalogProductionId,
    );

    //Ensure input value is converted to boolean if not null
    if (addSectionDetails) {
      addSectionDetails = Boolean(addSectionDetails);
    }

    return new VenueChartResponseDto(chart, addSectionDetails);
  }

  @Get('section-details')
  async GetSectionDetails(@Query(ValidationPipe) { id }) {
    const section = await this.service.GetSectionDetails(id);
    return new VenueChartSectionDto(section);
  }

  @Post('section-bulk-rename')
  async BulkSectionRename(
    @Body(ValidationPipe)
    {
      sectionIds,
      replacePattern,
      replaceValue,
      replaceType,
      removeOldAliases,
      addNameAlias,
    },
  ) {
    return await this.service.BulkSectionRename(
      sectionIds,
      replacePattern,
      replaceValue,
      replaceType,
      removeOldAliases,
      addNameAlias,
    );
  }

  @UseGuards(JwtAdminAuthGuard)
  @Post('chart-properties')
  async CreateVenueChart(@Body(ValidationPipe) newChart: CreateVenueChartDto) {
    return await this.service.CreateVenueChart(newChart);
  }

  @UseGuards(JwtAdminAuthGuard)
  @Post('section-details/:venueChartId')
  async CreateSectionDetails(
    @Param('venueChartId') venueChartId: string,
    @Body(ValidationPipe) newSection: CreateSectionDto,
  ) {
    return await this.service.CreateSectionDetails(
      Number(venueChartId),
      newSection,
    );
  }
}
