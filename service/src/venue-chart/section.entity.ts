import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { Row } from './row.entity';
import { VenueChart } from './venue-chart.entity';

@Entity()
export class Section {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('character varying')
  name: string;

  @Column('character varying', { nullable: true })
  map_section_id: string;

  @Column('jsonb', { nullable: true })
  chart_properties: object;

  @Column('character varying', { array: true, nullable: true })
  aliases: string[];

  @ManyToOne(() => VenueChart, (p) => p.sections, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'venue_chart_id' })
  chart: VenueChart;

  @Column()
  @RelationId((c: Section) => c.chart)
  venue_chart_id: number;

  @OneToMany(() => Row, (r) => r.section)
  rows: Row[];
}
