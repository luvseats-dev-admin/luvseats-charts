import { Test, TestingModule } from '@nestjs/testing';
import { VenueChartService } from './venue-chart.service';

describe('VenueChartService', () => {
  let service: VenueChartService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VenueChartService],
    }).compile();

    service = module.get<VenueChartService>(VenueChartService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
