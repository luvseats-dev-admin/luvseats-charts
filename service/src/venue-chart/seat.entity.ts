import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { Row } from './row.entity';

@Entity()
export class Seat {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('character varying')
  name: string;

  @Column('character varying', { nullable: true })
  map_seat_id: string;

  @Column('jsonb', { nullable: true })
  chart_properties: object;

  @ManyToOne(() => Row, (r) => r.seats, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'row_id' })
  row: Row;

  @Column()
  @RelationId((c: Seat) => c.row)
  row_id: number;
}
