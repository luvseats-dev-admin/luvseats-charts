import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { ProductionConfiguration } from '../production-configuration/production-configuration.entity';
import { VenueConfiguration } from '../venue-configuration/venue-configuration.entity';
import { Section } from './section.entity';

@Entity()
export class VenueChart {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('character varying', { unique: true })
  chart_name: string;

  @Column('character varying', { nullable: true })
  background_image: string;

  @Column('jsonb', { nullable: true })
  chart_properties: object;

  @OneToMany(() => Section, (s) => s.chart)
  sections: Section[];

  @OneToMany(() => ProductionConfiguration, (p) => p.chart)
  configurations: ProductionConfiguration[];

  @ManyToOne(() => VenueConfiguration, (c) => c.charts)
  @JoinColumn({ name: 'venue_id' })
  venue: VenueConfiguration;

  @Column()
  @RelationId((c: VenueChart) => c.venue)
  venue_id: number;
}
