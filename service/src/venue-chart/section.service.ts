import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from 'luvseats-crud-typeorm';
import { Repository } from 'typeorm';
import { Section } from './section.entity';

@Injectable()
export class SectionService extends TypeOrmCrudService<Section> {
  constructor(
    @InjectRepository(Section)
    public repo: Repository<Section>,
  ) {
    super(repo);
  }
}
