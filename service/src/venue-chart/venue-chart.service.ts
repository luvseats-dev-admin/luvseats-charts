import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from 'luvseats-crud-typeorm';
import { Repository } from 'typeorm';
import { ProductionConfiguration } from '../production-configuration/production-configuration.entity';
import { TransactionalRepository } from '../utils/dbutils/TransactionalRepository.provider';
import { UnitOfWork } from '../utils/dbutils/UnitOfWork.provider';
import { VenueConfiguration } from '../venue-configuration/venue-configuration.entity';
import {
  CreateRowDto,
  CreateSeatDto,
  CreateSectionDto,
  CreateVenueChartDto,
} from './dto/create-venue-chart.dto';
import { Row } from './row.entity';
import { Seat } from './seat.entity';
import { Section } from './section.entity';
import { VenueChart } from './venue-chart.entity';

@Injectable()
export class VenueChartService extends TypeOrmCrudService<VenueChart> {
  constructor(
    @InjectRepository(VenueChart) public repo: Repository<VenueChart>,
    @InjectRepository(Section) private sectionRepo: Repository<Section>,
    @InjectRepository(ProductionConfiguration)
    private productionRepo: Repository<ProductionConfiguration>,
    @InjectRepository(VenueConfiguration)
    private venueRepo: Repository<VenueConfiguration>,
    private readonly txRepo: TransactionalRepository,
    private readonly uow: UnitOfWork,
  ) {
    super(repo);
  }

  async GetVenueChartById(
    id?: number,
    catalogVenueId?: number,
    catalogProductionId?: number,
  ) {
    let chart: VenueChart;
    if (id) {
      chart = await this.repo.findOne({
        where: { id },
        relations: ['sections'],
      });
    }

    if (!chart && catalogProductionId) {
      const productionChart = await this.productionRepo.findOne({
        where: { catalog_production_id: catalogProductionId },
        relations: ['chart', 'chart.sections'],
      });
      if (productionChart?.chart) {
        chart = productionChart.chart;
      }
    }

    if (!chart && catalogVenueId) {
      const venueConfiguration = await this.venueRepo.findOne({
        where: { catalog_venue_id: catalogVenueId },
        relations: ['default_chart', 'default_chart.sections'],
      });
      if (!venueConfiguration) {
        throw new HttpException('No Chart Found', HttpStatus.NOT_FOUND);
      }
      chart = venueConfiguration.default_chart;
    }

    if (!chart) {
      throw new HttpException('No Chart Found', HttpStatus.NOT_FOUND);
    }

    return chart;
  }

  async GetSectionDetails(sectionId: number) {
    const section = await this.sectionRepo.findOne({
      where: { id: sectionId },
      relations: ['rows', 'rows.seats'],
    });

    if (!section) {
      throw new HttpException('No Chart Found', HttpStatus.NOT_FOUND);
    }

    return section;
  }

  async CreateVenueChart(newData: CreateVenueChartDto) {
    const response = await this.uow.withTransaction(async () => {
      const chartRepo = this.txRepo.getRepository(VenueChart);
      const sectionRepo = this.txRepo.getRepository(Section);
      let newChart = await chartRepo.findOne({
        where: { chart_name: newData.chartName },
      });
      if (!newChart) {
        newChart = chartRepo.create();
      } else {
        await this.RemoveChartDetails(newChart.id, true);
      }

      CreateVenueChartDto.MapToDbEntity(newChart, newData);
      const chartSaved = await chartRepo.save(newChart);
      chartSaved.sections = newData.sections?.map((x) =>
        CreateSectionDto.MapToDbEntity(x, chartSaved),
      );

      await sectionRepo.save(chartSaved.sections);
      return chartSaved;
    });

    return response;
  }

  async CreateSectionDetails(
    venueChartId: number,
    newSection: CreateSectionDto,
  ) {
    if (!venueChartId) {
      throw new HttpException(
        'Venue Chart Id Invalid or Not provided',
        HttpStatus.BAD_REQUEST,
      );
    }
    const response = await this.uow.withTransaction(async () => {
      const chartRepo = this.txRepo.getRepository(VenueChart);
      const sectionRepo = this.txRepo.getRepository(Section);
      const rowRepo = this.txRepo.getRepository(Row);
      const seatRepo = this.txRepo.getRepository(Seat);

      const chart = await chartRepo.findOne({ where: { id: venueChartId } });
      if (!chart) {
        throw new HttpException('Venue Chart Not Found!', HttpStatus.NOT_FOUND);
      }
      let savedSection = await sectionRepo.findOne({
        where: { name: newSection.dataSectionName, venue_chart_id: chart.id },
      });
      if (!savedSection) {
        savedSection = CreateSectionDto.MapToDbEntity(newSection, chart);
      } else {
        if (newSection.properties) {
          savedSection.chart_properties = newSection.properties;
        }
        await this.RemoveSectionDetails(savedSection.id);
      }
      savedSection = await sectionRepo.save(savedSection);

      savedSection.rows = newSection.rows?.map((x) =>
        CreateRowDto.MapToDbEntity(x, savedSection),
      );

      const savedRows = await rowRepo.save(savedSection.rows);
      let allSeats: Seat[] = [];
      savedRows.forEach((x) => {
        x.seats = newSection.rows
          .find((r) => r.name === x.name)
          .seats.map((s) => CreateSeatDto.MapToDbEntity(s, x));
        allSeats = allSeats.concat(x.seats);
      });
      allSeats = await seatRepo.save(allSeats);

      return savedSection;
    });

    return response;
  }

  async RemoveChartDetails(chartId: number, removeSections: boolean) {
    const response = await this.uow.withTransaction(async () => {
      const qb = this.txRepo
        .getRepository(VenueChart)
        .manager.createQueryBuilder();

      await qb
        .delete()
        .from(Seat)
        .where(
          '"row_id" IN (SELECT r.id FROM "row" as r INNER JOIN "section" as s ON r.section_id = s.id WHERE s.venue_chart_id = :id)',
          { id: chartId },
        )
        .execute();

      await qb
        .delete()
        .from(Row)
        .where(
          'section_id IN (SELECT id FROM "section" WHERE venue_chart_id = :id)',
          { id: chartId },
        )
        .execute();

      if (removeSections) {
        await qb
          .delete()
          .from(Section)
          .where('venue_chart_id = :id', { id: chartId })
          .execute();
      }
    });

    return response;
  }

  async RemoveSectionDetails(sectionId: number) {
    const qb = this.txRepo
      .getRepository(VenueChart)
      .manager.createQueryBuilder();

    await qb
      .delete()
      .from(Seat)
      .where(
        '"row_id" IN (SELECT r.id FROM "row" as r WHERE r.section_id = :id)',
        { id: sectionId },
      )
      .execute();

    await qb
      .delete()
      .from(Row)
      .where('section_id = :id', { id: sectionId })
      .execute();
  }

  async BulkSectionRename(
    sectionIds: string[],
    replacePattern: string,
    replaceValue: string,
    replaceType: string,
    removeOldAliases: boolean,
    addNameAlias: boolean,
  ) {
    const response = await this.uow.withTransaction(async () => {
      const sectionRepo = this.txRepo.getRepository(Section);
      const sections = await sectionRepo.findByIds(sectionIds);
      let valueCounter = Number(replaceValue);

      for (let i = 0; i < sectionIds.length; i++) {
        const section = sections.find((x) => x.id === Number(sectionIds[i]));
        switch (replaceType) {
          case 'replace':
            section.name = section.name.replace(replacePattern, replaceValue);
            break;
          case 'start':
            section.name = replaceValue + section.name;
            break;
          case 'end':
            section.name = section.name + replaceValue;
            break;
          case 'remove-end':
            section.name = section.name.split(replacePattern)[0];
            break;
          case 'value':
            section.name = valueCounter.toLocaleString('en-US', {
              minimumIntegerDigits: replacePattern.length,
              useGrouping: false,
            });
            valueCounter++;
            break;
        }

        if (removeOldAliases || !section.aliases) {
          section.aliases = [];
        }

        if (addNameAlias) {
          section.aliases.push(section.name);
        }

        await sectionRepo.save(section);
      }

      return sections;
    });

    return response;
  }
}
