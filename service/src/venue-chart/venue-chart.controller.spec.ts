import { Test, TestingModule } from '@nestjs/testing';
import { VenueChartController } from './venue-chart.controller';

describe('VenueChartController', () => {
  let controller: VenueChartController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VenueChartController],
    }).compile();

    controller = module.get<VenueChartController>(VenueChartController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
