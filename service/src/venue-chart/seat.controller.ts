import { Controller, UseGuards } from '@nestjs/common';
import { Crud, CrudController } from '@rewiko/crud';
import JwtAdminAuthGuard from '../admin-auth/jwt.admin-auth.guard';
import { Seat } from './seat.entity';
import { SeatService } from './seat.service';

@Crud({
  model: {
    type: Seat,
  },
  query: {
    join: {
      row: {
        eager: true,
      },
      'row.section': {
        eager: true,
      },
    },
  },
})
@Controller('seat')
@UseGuards(JwtAdminAuthGuard)
export class SeatController implements CrudController<Seat> {
  constructor(public service: SeatService) {}
}
