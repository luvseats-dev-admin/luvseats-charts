import { Controller, UseGuards } from '@nestjs/common';
import { Crud, CrudController } from '@rewiko/crud';
import JwtAdminAuthGuard from '../admin-auth/jwt.admin-auth.guard';
import { Section } from './section.entity';
import { SectionService } from './section.service';

@UseGuards(JwtAdminAuthGuard)
@Crud({
  model: {
    type: Section,
  },
})
@Controller('section')
export class SectionController implements CrudController<Section> {
  constructor(public service: SectionService) {}
}
