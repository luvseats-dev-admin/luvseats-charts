import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductionConfiguration } from '../production-configuration/production-configuration.entity';
import { VenueConfiguration } from '../venue-configuration/venue-configuration.entity';
import { Row } from './row.entity';
import { SeatController } from './seat.controller';
import { Seat } from './seat.entity';
import { SeatService } from './seat.service';
import { SectionController } from './section.controller';
import { Section } from './section.entity';
import { SectionService } from './section.service';
import { VenueChartController } from './venue-chart.controller';
import { VenueChart } from './venue-chart.entity';
import { VenueChartService } from './venue-chart.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      VenueChart,
      VenueConfiguration,
      ProductionConfiguration,
      Section,
      Row,
      Seat,
    ]),
  ],
  controllers: [VenueChartController, SectionController, SeatController],
  providers: [VenueChartService, SectionService, SeatService],
  exports: [VenueChartService, SectionService, SeatService],
})
export class VenueChartModule {}
