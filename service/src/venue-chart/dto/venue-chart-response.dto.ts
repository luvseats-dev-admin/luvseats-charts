import { Row } from '../row.entity';
import { Seat } from '../seat.entity';
import { Section } from '../section.entity';
import { VenueChart } from '../venue-chart.entity';

export class VenueChartResponseDto {
  constructor(input: VenueChart, addSectionDetails: boolean = true) {
    this.id = input.id;
    this.name = input.chart_name;
    this.backgroundImage = input.background_image;
    this.properties = input.chart_properties;
    this.sections = input.sections.map(
      (x) => new VenueChartSectionDto(x, addSectionDetails),
    );
  }

  id: Number;
  name: string;
  backgroundImage: string;
  properties: object;
  sections: VenueChartSectionDto[];
}

export class VenueChartSectionDto {
  constructor(input: Section, addSectionDetails: boolean = true) {
    this.id = input.id;
    this.dataSectionId = input.map_section_id;
    this.dataSectionName = input.name;
    this.aliases = input.aliases;
    this.properties = addSectionDetails ? input.chart_properties : undefined;
    if (input.rows) {
      this.rows = input.rows.map((x) => new VenueChartRowDto(x));
    }
  }

  id: Number;
  dataSectionId: string;
  dataSectionName: string;
  properties: object;
  aliases: string[];
  rows?: VenueChartRowDto[];
}

export class VenueChartRowDto {
  constructor(input: Row) {
    this.id = input.id;
    this.name = input.name;
    this.seats = input?.seats?.map((x) => new VenueChartSeatDto(x)) ?? [];
    this.properties = input?.chart_properties;
  }

  id: Number;
  name: string;
  properties: object;
  seats: VenueChartSeatDto[];
}

export class VenueChartSeatDto {
  constructor(input: Seat) {
    this.id;
    this.name = input.name;
    this.properties = input.chart_properties;
  }

  id: Number;
  name: string;
  properties: object;
}
