export class GetVenueChartDto {
  id?: number;
  catalogVenueId?: number;
  catalogProductionId?: number;
  addSectionDetails?: boolean;
}
