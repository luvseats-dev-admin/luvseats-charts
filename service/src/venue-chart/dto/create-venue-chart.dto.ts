import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import { Row } from '../row.entity';
import { Seat } from '../seat.entity';
import { Section } from '../section.entity';
import { VenueChart } from '../venue-chart.entity';

export class CreateVenueChartDto {
  @IsString()
  @IsNotEmpty()
  chartName: string;

  @IsString()
  @IsOptional()
  backgroundImage: string;

  @IsNumber()
  @IsNotEmpty()
  venueConfigurationId: number;

  @IsObject()
  @IsOptional()
  properties: object;

  @IsArray()
  sections: CreateSectionDto[];

  static MapToDbEntity(input: VenueChart, dto: CreateVenueChartDto) {
    input.chart_name = dto.chartName;
    input.background_image = dto.backgroundImage;
    input.venue_id = dto.venueConfigurationId;
    input.chart_properties = dto.properties;
  }
}

export class CreateSectionDto {
  @IsString()
  @IsOptional()
  dataSectionName: string;

  @IsString()
  @IsNotEmpty()
  dataSectionId: string;

  @IsObject()
  @IsOptional()
  properties: object;

  @IsArray()
  @IsOptional()
  aliases: string[];

  @IsArray()
  @IsOptional()
  rows: CreateRowDto[];

  static MapToDbEntity(input: CreateSectionDto, venueChart: VenueChart) {
    const newSection = new Section();
    newSection.name = input.dataSectionName;
    newSection.map_section_id = input.dataSectionId;
    newSection.chart_properties = input.properties;
    newSection.venue_chart_id = venueChart.id;
    newSection.aliases = input.aliases;
    return newSection;
  }
}

export class CreateRowDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsOptional()
  dataRowId: string;

  @IsObject()
  @IsOptional()
  properties: object;

  @IsArray()
  @IsOptional()
  seats: CreateSeatDto[];

  static MapToDbEntity(input: CreateRowDto, section: Section) {
    const newRow = new Row();
    newRow.name = input.name;
    newRow.chart_properties = input.properties;
    newRow.map_row_id = input.dataRowId;
    newRow.section_id = section.id;
    return newRow;
  }
}

export class CreateSeatDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsOptional()
  dataSeatId: string;

  @IsObject()
  @IsOptional()
  properties: object;

  static MapToDbEntity(input: CreateSeatDto, row: Row) {
    const newSeat = new Seat();
    newSeat.name = input.name;
    newSeat.map_seat_id = input.dataSeatId;
    newSeat.chart_properties = input.properties;
    newSeat.row_id = row.id;
    return newSeat;
  }
}
