import { Test, TestingModule } from '@nestjs/testing';
import { ProductionConfigurationController } from './production-configuration.controller';

describe('ProductionConfigurationController', () => {
  let controller: ProductionConfigurationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductionConfigurationController],
    }).compile();

    controller = module.get<ProductionConfigurationController>(ProductionConfigurationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
