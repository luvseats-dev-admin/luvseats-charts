import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { VenueChart } from '../venue-chart/venue-chart.entity';

@Entity()
export class ProductionConfiguration {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('character varying')
  production_name: string;

  @Column('integer', { nullable: true })
  catalog_production_id: number;

  @Column('character varying', { default: 'INACTIVE' })
  status: string;

  @ManyToOne(() => VenueChart, (v) => v.configurations)
  @JoinColumn({ name: 'venue_chart_id' })
  chart: VenueChart;

  @Column()
  @RelationId((c: ProductionConfiguration) => c.chart)
  venue_chart_id: number;
}
