import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from 'luvseats-crud-typeorm';
import { Repository } from 'typeorm';
import { ProductionConfiguration } from './production-configuration.entity';

@Injectable()
export class ProductionConfigurationService extends TypeOrmCrudService<ProductionConfiguration> {
  constructor(
    @InjectRepository(ProductionConfiguration)
    public repo: Repository<ProductionConfiguration>,
  ) {
    super(repo);
  }

  async bulkAssign(
    venueChartId: number,
    productions: { id: number; name: string }[],
  ) {
    await this.repo
      .createQueryBuilder()
      .delete()
      .where('catalog_production_id in (:...productionIds)', {
        productionIds: productions.map((p) => p.id),
      })
      .execute();

    const valuesToInsert = productions.map((p) => ({
      venue_chart_id: venueChartId,
      catalog_production_id: p.id,
      production_name: p.name,
      status: 'ACTIVE',
    }));
    const newEntities = await this.repo.insert(valuesToInsert);

    return newEntities;
  }
}
