import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductionConfigurationController } from './production-configuration.controller';
import { ProductionConfiguration } from './production-configuration.entity';
import { ProductionConfigurationService } from './production-configuration.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProductionConfiguration])],
  controllers: [ProductionConfigurationController],
  providers: [ProductionConfigurationService],
})
export class ProductionConfigurationModule {}
