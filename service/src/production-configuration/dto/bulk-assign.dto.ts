export class BulkAssignDto {
  venueChartId: number;
  productions: { id: number; name: string }[];
}
