import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import { Crud, CrudController } from '@rewiko/crud';
import { BulkAssignDto } from './dto/bulk-assign.dto';
import { ProductionConfiguration } from './production-configuration.entity';
import { ProductionConfigurationService } from './production-configuration.service';

@Crud({
  model: {
    type: ProductionConfiguration,
  },
})
@Controller('production-configuration')
export class ProductionConfigurationController
  implements CrudController<ProductionConfiguration> {
  constructor(public service: ProductionConfigurationService) {}

  @Post('bulk-assign')
  async bulkAssign(@Body(ValidationPipe) data: BulkAssignDto) {
    return this.service.bulkAssign(data.venueChartId, data.productions);
  }
}
