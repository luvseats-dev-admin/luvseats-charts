import { Module, Global } from '@nestjs/common';
import { UnitOfWork } from './UnitOfWork.provider';
import { TransactionalRepository } from './TransactionalRepository.provider';

@Global()
@Module({
  providers: [UnitOfWork, TransactionalRepository],
  exports: [UnitOfWork, TransactionalRepository],
})
export class UnitOfWorkModule {}
