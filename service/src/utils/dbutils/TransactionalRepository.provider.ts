import { Injectable } from '@nestjs/common';
import {
  getRepository,
  Repository,
  EntitySchema,
  ObjectType,
  DataSource,
} from 'typeorm';
// import { RepositoryFactory } from 'typeorm/repository/Repository';
import { UnitOfWork } from './UnitOfWork.provider';

@Injectable()
export class TransactionalRepository {
  constructor(private uow: UnitOfWork, private dataSource: DataSource) {}

  /**
   * Gets a repository bound to the current transaction manager
   * or defaults to the current connection's call to getRepository().
   */
  getRepository<Entity>(
    target: ObjectType<Entity> | EntitySchema<Entity> | string,
  ): any {
    const transactionManager = this.uow.getTransactionManager();
    if (transactionManager) {
      const dataSource = this.uow.getDataSource();
      const metadata = dataSource.getMetadata(target);

      return transactionManager.getRepository(target);
    }

    return this.dataSource.getRepository(target);
  }
}
