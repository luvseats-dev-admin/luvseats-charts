import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { EntityManager } from 'typeorm';

@Injectable()
export class UnitOfWork {
  private transactionManager: EntityManager | null;

  constructor(private dataSource: DataSource) {}

  getTransactionManager(): EntityManager | null {
    return this.transactionManager;
  }

  getDataSource(): DataSource {
    return this.dataSource;
  }

  async withTransaction<T>(work: () => T): Promise<T> {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.startTransaction();
    this.transactionManager = queryRunner.manager;

    try {
      const result = await work();
      await queryRunner.commitTransaction();
      return result;
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
      this.transactionManager = null;
    }
  }
}
