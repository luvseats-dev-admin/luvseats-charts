import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from 'nestjs-pino';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.setGlobalPrefix('api/chart-service');
  const logger = app.get(Logger);
  app.useLogger(logger);
  const configService = app.get(ConfigService);

  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  const cors = configService.get<string>('appConfig.corsAppServer');
  logger.log(`CORS enabled for server ${cors}`);
  app.enableCors({ origin: cors });

  const port = configService.get<number>('appConfig.port');

  logger.log(`Starting Application on port: ${port}`);

  await app.listen(port);
}
bootstrap();
