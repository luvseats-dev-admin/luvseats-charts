import { Test, TestingModule } from '@nestjs/testing';
import { VenueConfigurationController } from './venue-configuration.controller';

describe('VenueConfigurationController', () => {
  let controller: VenueConfigurationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VenueConfigurationController],
    }).compile();

    controller = module.get<VenueConfigurationController>(VenueConfigurationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
