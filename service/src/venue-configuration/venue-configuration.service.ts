import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from 'luvseats-crud-typeorm';
import { Repository } from 'typeorm';
import { Row } from '../venue-chart/row.entity';
import { Seat } from '../venue-chart/seat.entity';
import { Section } from '../venue-chart/section.entity';
import { VenueChart } from '../venue-chart/venue-chart.entity';
import { VenueChartService } from '../venue-chart/venue-chart.service';

import {
  ChartRowDto,
  ChartSectionDto,
  VenueChartCreateDto,
} from './dto/venue-chart-create.dto';
import { VenueConfiguration } from './venue-configuration.entity';

@Injectable()
export class VenueConfigurationService extends TypeOrmCrudService<VenueConfiguration> {
  constructor(
    @InjectRepository(VenueConfiguration)
    public repo: Repository<VenueConfiguration>,
    @InjectRepository(VenueChart)
    private chartRepo: Repository<VenueChart>,
    @InjectRepository(Section)
    private sectionRepo: Repository<Section>,
    @InjectRepository(Row)
    private rowRepo: Repository<Row>,
    @InjectRepository(Seat)
    private seatRepo: Repository<Seat>,
    private chartService: VenueChartService,
  ) {
    super(repo);
  }

  async CreateUpdateVenueChart(input: VenueChartCreateDto) {
    let venue = await this.repo.findOne({
      where: { venue_name: input.venueName },
      relations: ['default_chart'],
    });
    if (!venue) {
      venue = this.repo.create();
      venue.catalog_venue_id = input.catalogVenueId;
      venue.venue_name = input.venueName;
      venue = await this.repo.save(venue);
    } else {
      if (
        input.catalogVenueId &&
        venue.catalog_venue_id != input.catalogVenueId
      ) {
        venue.catalog_venue_id = input.catalogVenueId;
        await this.repo.save(venue);
      }
    }
    let chart: VenueChart;
    if (venue.default_chart) {
      chart = await this.chartRepo.findOne({
        where: { id: venue.default_chart.id },
      });
      await this.chartService.RemoveChartDetails(chart.id, true);
    } else {
      chart = this.chartRepo.create();
    }

    chart.background_image = input.backgroundImage;
    chart.chart_name = input.configurationName;
    chart.chart_properties = input.properties;
    chart.venue_id = venue.id;

    chart = await this.chartRepo.save(chart);
    venue.default_chart = chart;
    venue = await this.repo.save(venue);

    chart.sections = input.sections.map((x) =>
      ChartSectionDto.mapToEntity(x, chart),
    );
    chart.sections = await this.sectionRepo.save(chart.sections);

    let allRows: Row[] = [];
    chart.sections.forEach((x) => (allRows = allRows.concat(x.rows)));
    allRows = await this.rowRepo.save(allRows);

    let allSeats: Seat[] = [];
    allRows.forEach((x) => (allSeats = allSeats.concat(x.seats)));
    allSeats = await this.seatRepo.save(allSeats);
    return venue;
  }

  async GetVenueConfig(id: number) {
    this.repo.findOne({
      where: { id },
      relations: ['default_chart', 'default_chart.sections'],
    });
  }

  async UpdateSectionData(
    chartName: string,
    mapSectionId: string,
    rows: ChartRowDto[],
  ) {
    const chart = await this.chartRepo.findOne({
      where: { chart_name: chartName },
    });
    if (!chart) {
      throw new HttpException('Chart Not Found!', HttpStatus.NOT_FOUND);
    }

    const section = await this.sectionRepo.findOne({
      where: { venue_chart_id: chart.id, map_section_id: mapSectionId },
      relations: ['rows', 'rows.seats'],
    });

    if (section?.rows.length > 0) {
      let savedSeats = [];
      section.rows.forEach(
        (x) => (savedSeats = savedSeats.concat(x.seats.map((y) => y.id))),
      );
      await this.seatRepo.delete(savedSeats);
      await this.rowRepo.delete(section.rows.map((x) => x.id));
    }

    const mappedRows = rows.map((x) => ChartRowDto.mapToEntity(x, section));
    const savedRows = await this.rowRepo.save(mappedRows);

    let allSeats: Seat[] = [];
    savedRows.forEach((x) => (allSeats = allSeats.concat(x.seats)));
    allSeats = await this.seatRepo.save(allSeats);

    return mappedRows;
  }
}
