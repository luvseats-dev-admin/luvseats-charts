import { IsNotEmpty, IsString } from 'class-validator';

export class UploadDataDto {
  @IsString()
  @IsNotEmpty()
  venueName: string;

  @IsString()
  @IsNotEmpty()
  configurationName: string;

  catalogVenueId?: number;
}
