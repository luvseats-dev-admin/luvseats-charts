import { IsNotEmpty, IsString } from 'class-validator';

export class SectionDetailDataDto {
  @IsString()
  @IsNotEmpty()
  configurationName: string;

  @IsNotEmpty()
  sectionData: any;

  @IsString()
  @IsNotEmpty()
  sectionId: string;
}
