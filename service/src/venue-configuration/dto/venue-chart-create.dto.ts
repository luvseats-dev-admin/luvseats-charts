import { Row } from '../../venue-chart/row.entity';
import { Seat } from '../../venue-chart/seat.entity';
import { Section } from '../../venue-chart/section.entity';
import { VenueChart } from '../../venue-chart/venue-chart.entity';

export class VenueChartCreateDto {
  venueName: string;
  configurationName: string;
  catalogVenueId?: number;
  backgroundImage?: string;
  properties: object;
  sections: ChartSectionDto[];
}

export class ChartSectionDto {
  dataSectionId: string;
  dataSectionName: string;
  d: string;
  labels: ChartLabelDto;
  rows: ChartRowDto[];

  static mapToEntity(input: ChartSectionDto, chart: VenueChart): Section {
    const newSection = new Section();
    newSection.chart = chart;
    newSection.name = input.dataSectionName;
    newSection.map_section_id = input.dataSectionId;
    newSection.aliases = [input.labels.value];
    newSection.chart_properties = {
      d: input.d,
      labels: input.labels,
    };

    newSection.rows =
      input?.rows?.map(x => ChartRowDto.mapToEntity(x, newSection)) ?? [];
    return newSection;
  }
}

export class ChartLabelDto {
  value: string;
  dy: string;
  x: number;
  y: number;
  'font-size': number;
}

export class ChartRowDto {
  name: string;
  seats: ChartSeatDto[];

  static mapToEntity(input: ChartRowDto, section: Section): Row {
    const newRow = new Row();
    newRow.name = input.name;
    newRow.section = section;

    newRow.seats =
      input?.seats?.map(x => ChartSeatDto.mapToEntity(x, newRow)) ?? [];
    return newRow;
  }
}

export class ChartSeatDto {
  name: string;
  cx: number;
  cy: number;
  r: number;

  static mapToEntity(input: ChartSeatDto, row: Row): Seat {
    const newSeat = new Seat();
    newSeat.name = input.name;
    newSeat.row = row;
    newSeat.chart_properties = {
      cx: input.cx,
      cy: input.cy,
      r: input.r,
    };
    return newSeat;
  }
}
