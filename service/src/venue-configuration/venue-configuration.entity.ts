import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { VenueChart } from '../venue-chart/venue-chart.entity';

@Entity()
export class VenueConfiguration {
  @PrimaryGeneratedColumn({ type: 'integer' })
  id: number;

  @Column('integer', { nullable: true, unique: true })
  catalog_venue_id: number;

  @Column('character varying', { unique: true })
  venue_name: string;

  @OneToOne(() => VenueChart)
  @JoinColumn({ name: 'default_chart_id' })
  default_chart: VenueChart;

  @Column({ nullable: true })
  @RelationId((c: VenueConfiguration) => c.default_chart)
  default_chart_id: number | null;

  @OneToMany(() => VenueChart, (c) => c.venue)
  charts: VenueChart[];
}
