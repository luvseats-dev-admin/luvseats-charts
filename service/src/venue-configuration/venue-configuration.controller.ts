import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Crud, CrudController } from '@rewiko/crud';
import { parse } from 'svg-parser';
import JwtAdminAuthGuard from '../admin-auth/jwt.admin-auth.guard';
import { GetVenueConfigDto } from './dto/get-venue-config.dto';
import { SectionDetailDataDto } from './dto/section-detail-data.dto';
import { UploadDataDto } from './dto/upload-data.dto';
import {
  ChartSectionDto,
  VenueChartCreateDto,
} from './dto/venue-chart-create.dto';
import { VenueConfiguration } from './venue-configuration.entity';
import { VenueConfigurationService } from './venue-configuration.service';

@UseGuards(JwtAdminAuthGuard)
@Crud({
  model: {
    type: VenueConfiguration,
  },
})
@Controller('venue-configuration')
export class VenueConfigurationController
  implements CrudController<VenueConfiguration> {
  constructor(public service: VenueConfigurationService) {}

  @Post('create')
  async CreateVenueConfiguration(
    @Body(ValidationPipe) req: VenueChartCreateDto,
  ) {
    const data = await this.service.CreateUpdateVenueChart(req);
    return {
      msg: 'Venue Chart Created/Updated',
      venueConfigurationId: data.id,
      venueChartId: data.default_chart.id,
      numberOfSections: data.default_chart.sections.length,
      numberOfRows: data.default_chart.sections.reduce(
        (prev, curr) => prev + curr.rows.length,
        0,
      ),
      NumberOfSeats: data.default_chart.sections.reduce(
        (prev, curr) =>
          prev +
          curr.rows.reduce((sprev, scurr) => sprev + scurr.seats.length, 0),
        0,
      ),
    };
  }

  @Get('default')
  async GetDefaultVenueConfiguration(
    @Query(ValidationPipe) req: GetVenueConfigDto,
  ) {
    return await this.service.GetVenueConfig(req.id);
  }

  @Post('upload-map-data')
  @UseInterceptors(FileInterceptor('file'))
  async UploadVenueConfiguration(
    @Body(ValidationPipe) data: UploadDataDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const sectionMap = file.buffer.toString();
    const svgSectionsParsed = parse(sectionMap);
    const sectionMapData = this.getMapSectionData(svgSectionsParsed);
    const createData: VenueChartCreateDto = {
      configurationName: data.configurationName,
      venueName: data.venueName,
      catalogVenueId: null,
      backgroundImage: null,
      properties: sectionMapData.properties,
      sections: sectionMapData.sections,
    };
    const returnData = await this.service.CreateUpdateVenueChart(createData);
    return {
      msg: 'Venue Chart Created/Updated',
      venueConfigurationId: returnData.id,
      venueChartId: returnData.default_chart.id,
      numberOfSections: returnData.default_chart.sections.length,
    };
  }

  @Post('add-section-details')
  async AddSectionDetails(
    @Body(ValidationPipe) sectionData: SectionDetailDataDto,
  ) {
    const rows = this.setRows(sectionData.sectionData);
    const returnData = await this.service.UpdateSectionData(
      sectionData.configurationName,
      sectionData.sectionId,
      rows,
    );
  }

  private setSections = (
    polygons: { children: any[] },
    labels: { children: any[] },
    seats: { children: any[] },
  ) => {
    const mapped: ChartSectionDto[] = polygons?.children.map((p) => {
      return {
        dataSectionId: p.properties['data-section-id'],
        dataSectionName: p.properties['data-section-name'],
        d: p.properties['d'],
        labels: this.setLabel(
          labels?.children.find(
            (x) =>
              x.properties['data-label-id'] === p.properties['data-section-id'],
          ),
        ),
        rows: this.setRows(
          seats?.children.find(
            (x) =>
              x.properties['data-section-id'] ===
              p.properties['data-section-id'],
          ),
        ),
      };
    });
    return mapped;
  };

  private setRows = (rows: { children: any[] }) => {
    return rows?.children.map((x) => ({
      name: x.properties['data-row-name'],
      seats: this.setSeats(x.children),
    }));
  };

  private setSeats = (seats: any[]) => {
    return seats.map((x) => ({
      name: x.properties['data-seat-name'],
      cx: x.properties.cx,
      cy: x.properties.cy,
      r: 10,
    }));
  };

  private setLabel = (label: {
    properties: { [x: string]: any };
    children: {
      properties: { x: number; y: number; dy: string };
      children: { value: string }[];
    }[];
  }) => {
    return {
      'font-size': label.properties['font-size'],
      x: label.children[0].properties.x,
      y: label.children[0].properties.y,
      dy: label.children[0].properties.dy,
      value: label.children[0].children[0].value,
    };
  };

  private setProperties = (properties: any[]) => {
    const parsedNode = {
      viewBox: {
        x: properties[0],
        y: properties[1],
        width: properties[2],
        height: properties[3],
      },
    };
    return parsedNode;
  };

  private getMapSectionData: (
    input: any,
  ) => { properties: object; sections: ChartSectionDto[] } = function (input) {
    const svgNode = input.children[0];
    return {
      properties: this.setProperties(svgNode.properties.viewBox.split(' ')),
      sections: this.setSections(
        svgNode.children.find(
          (x: { properties: { class: string } }) =>
            x.properties.class === 'polygons',
        ),
        svgNode.children.find(
          (x: { properties: { class: string } }) =>
            x.properties.class === 'labels',
        ),
        svgNode.children.find(
          (x: { properties: { class: string } }) =>
            x.properties.class === 'seats',
        ),
      ),
    };
  };
}
