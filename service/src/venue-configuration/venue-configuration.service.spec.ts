import { Test, TestingModule } from '@nestjs/testing';
import { VenueConfigurationService } from './venue-configuration.service';

describe('VenueConfigurationService', () => {
  let service: VenueConfigurationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VenueConfigurationService],
    }).compile();

    service = module.get<VenueConfigurationService>(VenueConfigurationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
