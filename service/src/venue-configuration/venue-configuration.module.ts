import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductionConfiguration } from '../production-configuration/production-configuration.entity';
import { Row } from '../venue-chart/row.entity';
import { Seat } from '../venue-chart/seat.entity';
import { Section } from '../venue-chart/section.entity';
import { VenueChart } from '../venue-chart/venue-chart.entity';
import { VenueChartModule } from '../venue-chart/venue-chart.module';
import { VenueConfigurationController } from './venue-configuration.controller';
import { VenueConfiguration } from './venue-configuration.entity';
import { VenueConfigurationService } from './venue-configuration.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      VenueChart,
      VenueConfiguration,
      ProductionConfiguration,
      Section,
      Row,
      Seat,
    ]),
    VenueChartModule,
  ],
  controllers: [VenueConfigurationController],
  providers: [VenueConfigurationService],
})
export class VenueConfigurationModule {}
