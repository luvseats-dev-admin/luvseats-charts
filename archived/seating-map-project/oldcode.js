
import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Circle, G, Path, Svg, Image, Rect, Defs, Pattern, Ellipse, SvgXml, ClipPath, SvgWithCss } from 'react-native-svg';
import SvgUri from 'react-native-svg-uri-updated';
import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
import jsonData from "./seats-data.json";
import SelectDropdown from 'react-native-select-dropdown';

var obj = JSON.parse(JSON.stringify(jsonData));

const SVGHeight = 10500;
const SVGWidth = 7700;
const countries = ["Allegiant Stadium", "AT&T Stadium", "Bank of America Stadium", "Caesars Superdome"]

class TextInANest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      stadium: '',
      // data: [],
      // section: []
    };
  }
  componentDidMount() {
  }

  getData = (val) => {
    this.xhrRequest(val).then((data) => {
      this.setState({ data });
    });
  };
  xhrRequest = (val) => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET",
        headers: { "Content-Type": "application/json" },
        // params: JSON.stringify({val}),
      };
      fetch(`http://192.168.100.19:3001/getStadium/${val}`)
        .then((response) => response.json())
        .then((data) => {
          resolve(data[0].stadium);
        });
    });
  };

  getSections = (val) => {
    this.xhrRequestTwo(val).then((section) => {
      console.log(val);
      this.setState({ section });
    });
  };
  xhrRequestTwo = (val) => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET",
        headers: { "Content-Type": "application/json" },
        // params: JSON.stringify({val}),
      };
      fetch(`http://192.168.100.19:3001/getSection/${val}`)
        .then((response) => response.json())
        .then((data) => {
          resolve(data[0].sections);
        });
    });
  };

  toggle = () => {
    this.setState({ hover: !this.state.hover });
  };

  pressed = (e) => {
    console.log(this);
  };
  render() {
    return (
      <>
        <SvgPanZoom
          canvasHeight={500}
          canvasWidth={500}
          minScale={0.3}
          initialZoom={0.4}
          maxScale={3}
          onZoom={(zoom) => { console.log('onZoom:' + zoom) }}
          canvasStyle={{ backgroundColor: 'white' }}
          viewStyle={{ backgroundColor: 'white' }}
        >
          <Svg height={SVGHeight} width={SVGWidth}>
            <Defs>
              <ClipPath id="clip">
                <Circle cx="50%" cy="50%" r="40%" />
              </ClipPath>
            </Defs>
            <Rect x="0" y="0" width="100%" height="100%" fill="red" />
            <Rect x="5%" y="5%" width="50%" height="90%" />

            <Image
              x="5%"
              y="5%"
              width="100%"
              height="100%"
              preserveAspectRatio="xMidYMid slice"
              opacity="1"
              href={require('./background.png')}
            />
            <Text
              x="50"
              y="50"
              textAnchor="middle"
              fontWeight="bold"
              fontSize="16"
              fill="blue"
            >
              HOGWARTS
            </Text>
          </Svg>
          <SvgUri
            svgXmlData={this.state.data}
          />
          <SvgUri
            svgXmlData={this.state.section}
          />

        </SvgPanZoom>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{
            backgroundColor: 'lightgrey',
            flexGrow: 1,
          }}>
            <SelectDropdown
              data={countries}
              onSelect={(selectedItem, index) => {
                // console.log(selectedItem, index)
                // this.setState({ stadium: selectedItem })
                this.getData(selectedItem);
                this.getSections(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                // text represented after item is selected
                // if data array is an array of objects then return selectedItem.property to render after item is selected
                return selectedItem
              }}
              rowTextForSelection={(item, index) => {
                // text represented for each item in dropdown
                // if data array is an array of objects then return item.property to represent item in dropdown
                return item
              }}
            />
            {/* <Text>{this.state.stadium}</Text> */}
            <Text>{(this.state.section) ? this.state.section : 'Loading....'}</Text>
            {/* <Button title="Sections" onPress={this.getSections(this.state.stadium)} /> */}
            {/* <SvgXml xml={this.state.data} width="100%" height="100%" /> */}
          </View>
        </View>
      </>
    );
  }
}