/* eslint-disable react-native/no-inline-styles */

import React, { Component } from 'react';
import { Text, View, PanResponder, Dimensions } from 'react-native';
import { Circle, G, Path, Svg, Image, Rect, Defs, Pattern, Ellipse, TSpan } from 'react-native-svg';
import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
// import jsonData from "./seats-data.json";
import SelectDropdown from 'react-native-select-dropdown';
// var obj = JSON.parse(JSON.stringify(jsonData));
import ZoomableSvg from 'zoomable-svg'; // 1.0.0

const SVGHeight = 10240;
const SVGWidth = 7680;
let stadiumsList = [];
let paths = [];
let seats = [];
let rowsInSubSections = [];
let seatsInRows = [];
var stadiumImages = [
  {
    title: 'Allegiant-Stadium',
    file: require('./Allegiant-Stadium.png'),
  },
  {
    title: 'AT&T-Stadium',
    file: require('./AT&T-Stadium.png'),
  },
  {
    title: 'Bank-of-America-Stadium',
    file: require('./Bank-of-America-Stadium.png'),
  },
  {
    title: 'Caesars-Superdome',
    file: require('./Caesars-Superdome.png'),
  },
  {
    title: 'Empower-Field-At-Mile-High',
    file: require('./Empower-Field-At-Mile-High.png'),
  },
  {
    title: 'FirstEnergy-Stadium',
    file: require('./FirstEnergy-Stadium.png'),
  },
  {
    title: 'Ford-Field',
    file: require('./Ford-Field.png'),
  },
  {
    title: 'GEHA-Field-at-Arrowhead-Stadium',
    file: require('./GEHA-Field-at-Arrowhead-Stadium.png'),
  },
  {
    title: 'Gillette-Stadium',
    file: require('./Gillette-Stadium.png'),
  },
  {
    title: 'Hard-Rock-Stadium',
    file: require('./Hard-Rock-Stadium.png'),
  },
  {
    title: 'Heinz-Field',
    file: require('./Heinz-Field.png'),
  },
  {
    title: 'Highmark-Stadium',
    file: require('./Highmark-Stadium.png'),
  },
  {
    title: 'Lambeau-Field',
    file: require('./Lambeau-Field.png'),
  },
  {
    title: 'Levis-Stadium',
    file: require('./Levis-Stadium.png'),
  },
  {
    title: 'Lincoln-Financial-Field',
    file: require('./Lincoln-Financial-Field.png'),
  },
  {
    title: 'Lucas-Oil-Stadium',
    file: require('./Lucas-Oil-Stadium.png'),
  },
  {
    title: 'Lumen-Field',
    file: require('./Lumen-Field.png'),
  },
  {
    title: 'M&T-Bank-Stadium',
    file: require('./M&T-Bank-Stadium.png'),
  },
  {
    title: 'Mercedes-Benz-Stadium',
    file: require('./Mercedes-Benz-Stadium.png'),
  },
  {
    title: 'MetLife-Stadium',
    file: require('./MetLife-Stadium.png'),
  },
  {
    title: 'Nissan-Stadium',
    file: require('./Nissan-Stadium.png'),
  },
  {
    title: 'NRG-Stadium',
    file: require('./NRG-Stadium.png'),
  },
  {
    title: 'Raymond-James-Stadium',
    file: require('./Raymond-James-Stadium.png'),
  },
  {
    title: 'SoFi-Stadium',
    file: require('./SoFi-Stadium.png'),
  },
  {
    title: 'Soldier-Field',
    file: require('./Soldier-Field.png'),
  },
  {
    title: 'State-Farm-Stadium',
    file: require('./State-Farm-Stadium.png'),
  },
  {
    title: 'TIAA-Bank-Field',
    file: require('./TIAA-Bank-Field.png'),
  },
  {
    title: 'U.S.-Bank-Stadium',
    file: require('./U.S.-Bank-Stadium.png'),
  },
  {
    title: 'Allegiant-Stadium',
    file: require('./Allegiant-Stadium.png'),
  },
];

class TextInANest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sections: [],
      seats: [],
      stadiums: []
    };
    this.getStadiums();
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onPanResponderGrant: () => { },
      onPanResponderTerminate: () => { },
      onMoveShouldSetPanResponder: () => true,
      onStartShouldSetPanResponder: () => true,
      onShouldBlockNativeResponder: () => true,
      onPanResponderTerminationRequest: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onStartShouldSetPanResponderCapture: () => true,
      onPanResponderMove: evt => {
        const touches = evt.nativeEvent.touches;
        const length = touches.length;
        if (length === 1) {
          const [{ locationX, locationY }] = touches;
          this.processTouch(locationX, locationY);
        } else if (length === 2) {
          const [touch1, touch2] = touches;
          this.processPinch(
            touch1.locationX,
            touch1.locationY,
            touch2.locationX,
            touch2.locationY
          );
        }
      },
      onPanResponderRelease: () => {
        this.setState({
          isZooming: false,
          isMoving: false,
        });
      },
    });
  }

  onPressTitle = () => {
    this.setState({ titleText: "Bird's Nest [pressed]" });
  };
  getStadiums = (val) => {
    this.getAllStadiums().then((stadiums) => {
      this.setState({ stadiums });
    });
  }
  getAllStadiums = () => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET"
      };
      fetch(`http://192.168.1.7:3001/getStadiums`)
        .then((response) => response.json())
        .then((data) => {
          data.map((d, i) => {
            stadiumsList.push(d.name);
          })
          resolve(paths);
        })
    });
  };

  getSeats = (val, id) => {
    this.seatsRequest(val, id).then((seats) => {
      this.setState({ seats });
    });
  };
  totalseatsRequest = (val) => {
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.1.7:3001/getSeats/${val}`)
        .then((response) => response.json())
        .then((data) => {
          seats = [];
          data.seats[0].rows.map((d, i) => {
            seats.push(d);
          })
          resolve(seats);
        })
    });
  };

  seatsRequest = (val, id) => {
    // const data = {name:val, id:"s_71"};
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.1.7:3001/getTotalSeats/${val}/${id}`)
        .then((response) => response.json())
        .then((data) => {
          seats = [];
          data.seats[0].rows.map((d, i) => {
            seats.push(d);
          })
          resolve(seats);
        })
    });
  };
  getData = (val) => {
    this.xhrRequest(val).then((sections) => {
      this.setState({ sections });
    });
  };
  xhrRequest = (val) => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET"
      };
      fetch(`http://192.168.1.7:3001/getSection/${val}`)
        .then((response) => response.json())
        .then((data) => {
          paths = [];
          data.sect.sections.map((d, i) => {
            paths.push(d);
          })
          resolve(paths);
        })
    });
  };

  render() {
    const { width, height } = Dimensions.get('window');
    return (
      <>
        <View style={{ flex: 1, flexDirection: 'row' }}
        // {...this._panResponder.panHandlers}
        >
          <ZoomableSvg
          align="mid"
          width={width}
          height={height}
          viewBoxSize={65}
          svgRoot={({ transform }) => (
            <Svg
              width={width}
              height={height}
              // viewBox="0 0 65 65"
              preserveAspectRatio="xMinYMin meet">
              <G transform={transform}>
                <Rect x="0" y="0" width="65" height="65" fill="white" />
                <Circle cx="32" cy="32" r="4.167" fill="blue" />
                <Path
                  d="M55.192 27.87l-5.825-1.092a17.98 17.98 0 0 0-1.392-3.37l3.37-4.928c.312-.456.248-1.142-.143-1.532l-4.155-4.156c-.39-.39-1.076-.454-1.532-.143l-4.928 3.37a18.023 18.023 0 0 0-3.473-1.42l-1.086-5.793c-.103-.543-.632-.983-1.185-.983h-5.877c-.553 0-1.082.44-1.185.983l-1.096 5.85a17.96 17.96 0 0 0-3.334 1.393l-4.866-3.33c-.456-.31-1.142-.247-1.532.144l-4.156 4.156c-.39.39-.454 1.076-.143 1.532l3.35 4.896a18.055 18.055 0 0 0-1.37 3.33L8.807 27.87c-.542.103-.982.632-.982 1.185v5.877c0 .553.44 1.082.982 1.185l5.82 1.09a18.013 18.013 0 0 0 1.4 3.4l-3.31 4.842c-.313.455-.25 1.14.142 1.53l4.155 4.157c.39.39 1.076.454 1.532.143l4.84-3.313c1.04.563 2.146 1.02 3.3 1.375l1.096 5.852c.103.542.632.982 1.185.982h5.877c.553 0 1.082-.44 1.185-.982l1.086-5.796c1.2-.354 2.354-.82 3.438-1.4l4.902 3.353c.456.313 1.142.25 1.532-.142l4.155-4.154c.39-.39.454-1.076.143-1.532l-3.335-4.874a18.016 18.016 0 0 0 1.424-3.44l5.82-1.09c.54-.104.98-.633.98-1.186v-5.877c0-.553-.44-1.082-.982-1.185zM32 42.085c-5.568 0-10.083-4.515-10.083-10.086 0-5.568 4.515-10.084 10.083-10.084 5.57 0 10.086 4.516 10.086 10.083 0 5.57-4.517 10.085-10.086 10.085z"
                  fill="blue"
                />
              </G>
            </Svg>
          )}
        />
          {/* <SvgPanZoom
            canvasHeight={600}
            canvasWidth={600}
            minScale={0.5}
            initialZoom={0.5}
            maxScale={10}
            onZoom={(zoom) => { console.log('onZoom:' + zoom) }}
            canvasStyle={{ backgroundColor: 'white' }}
            viewStyle={{ backgroundColor: 'white' }}
          > */}
            <Svg viewBox={"0 0 " + SVGHeight + " " + SVGWidth}>
              {/* <G
              transform={{
                translateX: left * resolution,
                translateY: top * resolution,
                scale: zoom,
              }}> */}
              {stadiumImages.map(d => {
                if (d.title == this.state.name) {
                  return (
                    <Image
                      x="0"
                      y="0"
                      width="10200"
                      height="7700"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={d.file}
                    />
                  )
                }
              })}

              {this.state.sections.map((item, key) => (
                <Path
                  key={key}
                  d={item.path.d}
                  fill={(this.state.sectionSelected == item.sectionId) ? "yellow" : "none"}
                  stroke={(this.state.sectionSelected == item.sectionId) ? "blue" : "green"}
                  strokeWidth={(this.state.sectionSelected == item.sectionId) ? "40" : "20"}
                  onPress={() => {
                    this.setState({
                      sectionSelected: item.sectionId
                    })
                    this.getSeats(this.state.name.replace('-', ' '), item.sectionId);
                    // console.log("section = " + `${item.sectionId}`)
                  }}
                />
              )
              )}
              {seats.map((a, i) => {
                for (let k = 0; k < a.seats.length; k++) {
                  return (
                    <G>
                      <Circle cx={a.seats[k].cx} cy={a.seats[k].cy} r="50" fill="red" />
                    </G>
                  );
                }
                // console.log(a.seats);
                // <Circle cx={a.cx} cy={a.cy} r="1" fill="pink" />
                // const obj = { 'rowname': a.datarowname, 'seats': a.seats };
                // seatsInRows.push(a);
              })}
              {/* {seatsInRows.map((a, i) => {
                let seats = [];
                for (let k = 0; k < a.seats.length; k++) {
                  seats.push(
                      <Circle
                        // ref={circleRef}
                        key={a.seats[k].id + "000" + k}
                        cx={a.seats[k].cx}
                        cy={a.seats[k].cy}
                        r={10}
                        id={a.seats[k].sectionName + "-" + a.rowname + "-" + a.seats[k].dataseatname}
                        // className="circ"
                        // data-checked={false}
                        // onClick={handleSeatClick}
                        // onMouseEnter={handleHover}
                        // onMouseLeave={handleMouseLeave}
                      />
                  );
                }
                console.log(seats);
                return seats;
              })} */}
            </Svg>
          {/* </SvgPanZoom> */}
        <ZoomableSvg
          align="mid"
          width={width}
          height={height}
          // viewBoxSize={65}
          svgRoot={({ transform }) => (
            <Svg
              width={width}
              height={height}
              // viewBox="0 0 65 65"
              preserveAspectRatio="xMinYMin meet">
              <G transform={transform}>
                <Rect x="0" y="0" width="65" height="65" fill="white" />
                <Circle cx="32" cy="32" r="4.167" fill="blue" />
                <Path
                  d="M55.192 27.87l-5.825-1.092a17.98 17.98 0 0 0-1.392-3.37l3.37-4.928c.312-.456.248-1.142-.143-1.532l-4.155-4.156c-.39-.39-1.076-.454-1.532-.143l-4.928 3.37a18.023 18.023 0 0 0-3.473-1.42l-1.086-5.793c-.103-.543-.632-.983-1.185-.983h-5.877c-.553 0-1.082.44-1.185.983l-1.096 5.85a17.96 17.96 0 0 0-3.334 1.393l-4.866-3.33c-.456-.31-1.142-.247-1.532.144l-4.156 4.156c-.39.39-.454 1.076-.143 1.532l3.35 4.896a18.055 18.055 0 0 0-1.37 3.33L8.807 27.87c-.542.103-.982.632-.982 1.185v5.877c0 .553.44 1.082.982 1.185l5.82 1.09a18.013 18.013 0 0 0 1.4 3.4l-3.31 4.842c-.313.455-.25 1.14.142 1.53l4.155 4.157c.39.39 1.076.454 1.532.143l4.84-3.313c1.04.563 2.146 1.02 3.3 1.375l1.096 5.852c.103.542.632.982 1.185.982h5.877c.553 0 1.082-.44 1.185-.982l1.086-5.796c1.2-.354 2.354-.82 3.438-1.4l4.902 3.353c.456.313 1.142.25 1.532-.142l4.155-4.154c.39-.39.454-1.076.143-1.532l-3.335-4.874a18.016 18.016 0 0 0 1.424-3.44l5.82-1.09c.54-.104.98-.633.98-1.186v-5.877c0-.553-.44-1.082-.982-1.185zM32 42.085c-5.568 0-10.083-4.515-10.083-10.086 0-5.568 4.515-10.084 10.083-10.084 5.57 0 10.086 4.516 10.086 10.083 0 5.57-4.517 10.085-10.086 10.085z"
                  fill="blue"
                />
              </G>
            </Svg>
          )}
        />
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{
            backgroundColor: 'lightgrey',
            flexGrow: 1,
          }}>
            <SelectDropdown
              data={stadiumsList}
              onSelect={(selectedItem, index) => {
                this.setState({
                  name: selectedItem.replace(/\s+/g, '-'),
                })
                this.getData(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem
              }}
              rowTextForSelection={(item, index) => {
                return item
              }}
            />
            <Text>{(this.state.name) ? this.state.name : 'Loading....'}</Text>
          </View>
        </View>
      </>
    );
  }
}
export default TextInANest;