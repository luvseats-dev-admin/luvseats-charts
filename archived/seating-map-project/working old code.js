/* eslint-disable react-native/no-inline-styles */

import React, { Component } from 'react';
import { Text, View, PanResponder } from 'react-native';
import { Circle, G, Path, Svg, Image, Rect, Defs, Pattern, Ellipse, TSpan } from 'react-native-svg';
import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
// import jsonData from "./seats-data.json";
import SelectDropdown from 'react-native-select-dropdown';
// var obj = JSON.parse(JSON.stringify(jsonData));

const SVGHeight = 10240;
const SVGWidth = 7680;
let stadiumsList = [];
let paths = [];
let seats = [];
let rowsInSubSections = [];
let seatsInRows = [];
var stadiumImages = [
  {
    title: 'Allegiant-Stadium',
    file: require('./Allegiant-Stadium.png'),
  },
  {
    title: 'AT&T-Stadium',
    file: require('./AT&T-Stadium.png'),
  },
  {
    title: 'Bank-of-America-Stadium',
    file: require('./Bank-of-America-Stadium.png'),
  },
  {
    title: 'Caesars-Superdome',
    file: require('./Caesars-Superdome.png'),
  },
  {
    title: 'Empower-Field-At-Mile-High',
    file: require('./Empower-Field-At-Mile-High.png'),
  },
  {
    title: 'FirstEnergy-Stadium',
    file: require('./FirstEnergy-Stadium.png'),
  },
  {
    title: 'Ford-Field',
    file: require('./Ford-Field.png'),
  },
  {
    title: 'GEHA-Field-at-Arrowhead-Stadium',
    file: require('./GEHA-Field-at-Arrowhead-Stadium.png'),
  },
  {
    title: 'Gillette-Stadium',
    file: require('./Gillette-Stadium.png'),
  },
  {
    title: 'Hard-Rock-Stadium',
    file: require('./Hard-Rock-Stadium.png'),
  },
  {
    title: 'Heinz-Field',
    file: require('./Heinz-Field.png'),
  },
  {
    title: 'Highmark-Stadium',
    file: require('./Highmark-Stadium.png'),
  },
  {
    title: 'Lambeau-Field',
    file: require('./Lambeau-Field.png'),
  },
  {
    title: 'Levis-Stadium',
    file: require('./Levis-Stadium.png'),
  },
  {
    title: 'Lincoln-Financial-Field',
    file: require('./Lincoln-Financial-Field.png'),
  },
  {
    title: 'Lucas-Oil-Stadium',
    file: require('./Lucas-Oil-Stadium.png'),
  },
  {
    title: 'Lumen-Field',
    file: require('./Lumen-Field.png'),
  },
  {
    title: 'M&T-Bank-Stadium',
    file: require('./M&T-Bank-Stadium.png'),
  },
  {
    title: 'Mercedes-Benz-Stadium',
    file: require('./Mercedes-Benz-Stadium.png'),
  },
  {
    title: 'MetLife-Stadium',
    file: require('./MetLife-Stadium.png'),
  },
  {
    title: 'Nissan-Stadium',
    file: require('./Nissan-Stadium.png'),
  },
  {
    title: 'NRG-Stadium',
    file: require('./NRG-Stadium.png'),
  },
  {
    title: 'Raymond-James-Stadium',
    file: require('./Raymond-James-Stadium.png'),
  },
  {
    title: 'SoFi-Stadium',
    file: require('./SoFi-Stadium.png'),
  },
  {
    title: 'Soldier-Field',
    file: require('./Soldier-Field.png'),
  },
  {
    title: 'State-Farm-Stadium',
    file: require('./State-Farm-Stadium.png'),
  },
  {
    title: 'TIAA-Bank-Field',
    file: require('./TIAA-Bank-Field.png'),
  },
  {
    title: 'U.S.-Bank-Stadium',
    file: require('./U.S.-Bank-Stadium.png'),
  },
  {
    title: 'Allegiant-Stadium',
    file: require('./Allegiant-Stadium.png'),
  },
];

function calcDistance(x1, y1, x2, y2) {
  const dx = x1 - x2;
  const dy = y1 - y2;
  return Math.sqrt(dx * dx + dy * dy);
}

function middle(p1, p2) {
  return (p1 + p2) / 2;
}

function calcCenter(x1, y1, x2, y2) {
  return {
    x: middle(x1, x2),
    y: middle(y1, y2),
  };
}

class TextInANest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sections: [],
      seats: [],
      stadiums: [],
      zoom: 1,
      left: 0,
      top: 0,
    };
    this.getStadiums();
  }

  processPinch(x1, y1, x2, y2) {
    const distance = calcDistance(x1, y1, x2, y2);
    const { x, y } = calcCenter(x1, y1, x2, y2);

    if (!this.state.isZooming) {
      const { top, left, zoom } = this.state;
      this.setState({
        isZooming: true,
        initialX: x,
        initialY: y,
        initialTop: top,
        initialLeft: left,
        initialZoom: zoom,
        initialDistance: distance,
      });
    } else {
      const {
        initialX,
        initialY,
        initialTop,
        initialLeft,
        initialZoom,
        initialDistance,
      } = this.state;

      const touchZoom = distance / initialDistance;
      const dx = x - initialX;
      const dy = y - initialY;

      const left = (initialLeft + dx - x) * touchZoom + x;
      const top = (initialTop + dy - y) * touchZoom + y;
      const zoom = initialZoom * touchZoom;

      this.setState({
        zoom,
        left,
        top,
      });
    }
  }

  processTouch(x, y) {
    if (!this.state.isMoving || this.state.isZooming) {
      const { top, left } = this.state;
      this.setState({
        isMoving: true,
        isZooming: false,
        initialLeft: left,
        initialTop: top,
        initialX: x,
        initialY: y,
      });
    } else {
      const { initialX, initialY, initialLeft, initialTop } = this.state;
      const dx = x - initialX;
      const dy = y - initialY;
      this.setState({
        left: initialLeft + dx,
        top: initialTop + dy,
      });
    }
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onPanResponderGrant: () => { },
      onPanResponderTerminate: () => { },
      onMoveShouldSetPanResponder: () => true,
      onStartShouldSetPanResponder: () => true,
      onShouldBlockNativeResponder: () => true,
      onPanResponderTerminationRequest: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onStartShouldSetPanResponderCapture: () => true,
      onPanResponderMove: evt => {
        const touches = evt.nativeEvent.touches;
        const length = touches.length;
        if (length === 1) {
          const [{ locationX, locationY }] = touches;
          this.processTouch(locationX, locationY);
        } else if (length === 2) {
          const [touch1, touch2] = touches;
          this.processPinch(
            touch1.locationX,
            touch1.locationY,
            touch2.locationX,
            touch2.locationY
          );
        }
      },
      onPanResponderRelease: () => {
        this.setState({
          isZooming: false,
          isMoving: false,
        });
      },
    });
  }

  onPressTitle = () => {
    this.setState({ titleText: "Bird's Nest [pressed]" });
  };
  getStadiums = (val) => {
    this.getAllStadiums().then((stadiums) => {
      this.setState({ stadiums });
    });
  }
  getAllStadiums = () => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET"
      };
      fetch(`http://192.168.1.7:3001/getStadiums`)
        .then((response) => response.json())
        .then((data) => {
          data.map((d, i) => {
            stadiumsList.push(d.name);
          })
          resolve(paths);
        })
    });
  };

  getSeats = (val, id) => {
    this.seatsRequest(val, id).then((seats) => {
      this.setState({ seats });
    });
  };
  totalseatsRequest = (val) => {
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.1.7:3001/getSeats/${val}`)
        .then((response) => response.json())
        .then((data) => {
          seats = [];
          data.seats[0].rows.map((d, i) => {
            seats.push(d);
          })
          resolve(seats);
        })
    });
  };

  seatsRequest = (val, id) => {
    // const data = {name:val, id:"s_71"};
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.1.7:3001/getTotalSeats/${val}/${id}`)
        .then((response) => response.json())
        .then((data) => {
          seats = [];
          data.seats[0].rows.map((d, i) => {
            seats.push(d);
          })
          resolve(seats);
        })
    });
  };
  getData = (val) => {
    this.xhrRequest(val).then((sections) => {
      this.setState({ sections });
    });
  };
  xhrRequest = (val) => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET"
      };
      fetch(`http://192.168.1.7:3001/getSection/${val}`)
        .then((response) => response.json())
        .then((data) => {
          paths = [];
          data.sect.sections.map((d, i) => {
            paths.push(d);
          })
          resolve(paths);
        })
    });
  };

  render() {
    const viewBoxSize = 65;
    const { height, width } = this.props;
    const { left, top, zoom } = this.state;
    const resolution = viewBoxSize / Math.min(height, width);
    return (
      <>
        <View style={{ flex: 1, flexDirection: 'row' }}
        // {...this._panResponder.panHandlers}
        >
          {/* <SvgPanZoom
            canvasHeight={600}
            canvasWidth={600}
            minScale={0.5}
            initialZoom={0.5}
            maxScale={10}
            onZoom={(zoom) => { console.log('onZoom:' + zoom) }}
            canvasStyle={{ backgroundColor: 'white' }}
            viewStyle={{ backgroundColor: 'white' }}
          > */}
            <Svg viewBox={"0 0 " + SVGHeight + " " + SVGWidth}>
              {/* <G
              transform={{
                translateX: left * resolution,
                translateY: top * resolution,
                scale: zoom,
              }}> */}
              {stadiumImages.map(d => {
                if (d.title == this.state.name) {
                  return (
                    <Image
                      x="0"
                      y="0"
                      width="10200"
                      height="7700"
                      preserveAspectRatio="xMidYMid slice"
                      opacity="1"
                      href={d.file}
                    />
                  )
                }
              })}

              {this.state.sections.map((item, key) => (
                <Path
                  key={key}
                  d={item.path.d}
                  fill={(this.state.sectionSelected == item.sectionId) ? "yellow" : "none"}
                  stroke={(this.state.sectionSelected == item.sectionId) ? "blue" : "green"}
                  strokeWidth={(this.state.sectionSelected == item.sectionId) ? "40" : "20"}
                  onPress={() => {
                    this.setState({
                      sectionSelected: item.sectionId
                    })
                    this.getSeats(this.state.name.replace('-', ' '), item.sectionId);
                    // console.log("section = " + `${item.sectionId}`)
                  }}
                />
              )
              )}
              {seats.map((a, i) => {
                for (let k = 0; k < a.seats.length; k++) {
                  return (
                    <G>
                      <Circle cx={a.seats[k].cx} cy={a.seats[k].cy} r="50" fill="red" />
                    </G>
                  );
                }
                // console.log(a.seats);
                // <Circle cx={a.cx} cy={a.cy} r="1" fill="pink" />
                // const obj = { 'rowname': a.datarowname, 'seats': a.seats };
                // seatsInRows.push(a);
              })}
              {/* {seatsInRows.map((a, i) => {
                let seats = [];
                for (let k = 0; k < a.seats.length; k++) {
                  seats.push(
                      <Circle
                        // ref={circleRef}
                        key={a.seats[k].id + "000" + k}
                        cx={a.seats[k].cx}
                        cy={a.seats[k].cy}
                        r={10}
                        id={a.seats[k].sectionName + "-" + a.rowname + "-" + a.seats[k].dataseatname}
                        // className="circ"
                        // data-checked={false}
                        // onClick={handleSeatClick}
                        // onMouseEnter={handleHover}
                        // onMouseLeave={handleMouseLeave}
                      />
                  );
                }
                console.log(seats);
                return seats;
              })} */}
            </Svg>
          {/* </SvgPanZoom> */}
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{
            backgroundColor: 'lightgrey',
            flexGrow: 1,
          }}>
            <SelectDropdown
              data={stadiumsList}
              onSelect={(selectedItem, index) => {
                this.setState({
                  name: selectedItem.replace(/\s+/g, '-'),
                })
                this.getData(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem
              }}
              rowTextForSelection={(item, index) => {
                return item
              }}
            />
            <Text>{(this.state.name) ? this.state.name : 'Loading....'}</Text>
          </View>
        </View>  
      </>
    );
  }
}
export default TextInANest;