/* eslint-disable react-native/no-inline-styles */

import React, { Component } from 'react';
import { Button, Text, View, PanResponder, Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import { Circle, G, Path, Svg, Image, Rect, Defs, Pattern, Ellipse, TSpan } from 'react-native-svg';
import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
// import jsonData from "./seats-data.json";
import SelectDropdown from 'react-native-select-dropdown';
// var obj = JSON.parse(JSON.stringify(jsonData));
import ZoomableSvg from 'zoomable-svg'; // 1.0.0

const SVGHeight = 10240;
const SVGWidth = 7680;
let stadiumsList = [];
let paths = [];
let seats = [];
let rowsInSubSections = [];
let seatsInRows = [];
let zoomable;
var stadiumImages = [
  {
    title: 'Allegiant-Stadium',
    file: require('./Allegiant-Stadium.png'),
  },
  {
    title: 'ATT-Stadium',
    file: require('./ATT-Stadium.png'),
  },
  {
    title: 'Bank-of-America-Stadium',
    file: require('./Bank-of-America-Stadium.png'),
  },
  {
    title: 'Caesars-Superdome',
    file: require('./Caesars-Superdome.png'),
  },
  {
    title: 'Empower-Field-At-Mile-High',
    file: require('./Empower-Field-At-Mile-High.png'),
  },
  {
    title: 'FirstEnergy-Stadium',
    file: require('./FirstEnergy-Stadium.png'),
  },
  {
    title: 'Ford-Field',
    file: require('./Ford-Field.png'),
  },
  {
    title: 'GEHA-Field-at-Arrowhead-Stadium',
    file: require('./GEHA-Field-at-Arrowhead-Stadium.png'),
  },
  {
    title: 'Gillette-Stadium',
    file: require('./Gillette-Stadium.png'),
  },
  {
    title: 'Hard-Rock-Stadium',
    file: require('./Hard-Rock-Stadium.png'),
  },
  {
    title: 'Heinz-Field',
    file: require('./Heinz-Field.png'),
  },
  {
    title: 'Highmark-Stadium',
    file: require('./Highmark-Stadium.png'),
  },
  {
    title: 'Lambeau-Field',
    file: require('./Lambeau-Field.png'),
  },
  {
    title: 'Levis-Stadium',
    file: require('./Levis-Stadium.png'),
  },
  {
    title: 'Lincoln-Financial-Field',
    file: require('./Lincoln-Financial-Field.png'),
  },
  {
    title: 'Lucas-Oil-Stadium',
    file: require('./Lucas-Oil-Stadium.png'),
  },
  {
    title: 'Lumen-Field',
    file: require('./Lumen-Field.png'),
  },
  {
    title: 'M&T-Bank-Stadium',
    file: require('./M&T-Bank-Stadium.png'),
  },
  {
    title: 'Mercedes-Benz-Stadium',
    file: require('./Mercedes-Benz-Stadium.png'),
  },
  {
    title: 'MetLife-Stadium',
    file: require('./MetLife-Stadium.png'),
  },
  {
    title: 'Nissan-Stadium',
    file: require('./Nissan-Stadium.png'),
  },
  {
    title: 'NRG-Stadium',
    file: require('./NRG-Stadium.png'),
  },
  {
    title: 'Raymond-James-Stadium',
    file: require('./Raymond-James-Stadium.png'),
  },
  {
    title: 'SoFi-Stadium',
    file: require('./SoFi-Stadium.png'),
  },
  {
    title: 'Soldier-Field',
    file: require('./Soldier-Field.png'),
  },
  {
    title: 'State-Farm-Stadium',
    file: require('./State-Farm-Stadium.png'),
  },
  {
    title: 'TIAA-Bank-Field',
    file: require('./TIAA-Bank-Field.png'),
  },
  {
    title: 'U.S.-Bank-Stadium',
    file: require('./U.S.-Bank-Stadium.png'),
  },
  {
    title: 'Allegiant-Stadium',
    file: require('./Allegiant-Stadium.png'),
  },
];

class TextInANest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sections: [],
      seats: [],
      stadiums: [],
      toggle: false,
    };
    this.getStadiums();
  }

  getStadiums = (val) => {
    this.getAllStadiums().then((stadiums) => {
      this.setState({ stadiums });
    });
  }
  getAllStadiums = () => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET"
      };
      fetch(`http://192.168.1.7:3001/getStadiums`)
        .then((response) => response.json())
        .then((data) => {
          data.map((d, i) => {
            stadiumsList.push(d.name);
          })
          resolve(paths);
        })
    });
  };

  getSeats = (val, id) => {
    this.seatsRequest(val, id).then((seats) => {
      this.setState({ seats });
    });
  };
  totalseatsRequest = (val) => {
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.1.7:3001/getSeats/${val}`)
        .then((response) => response.json())
        .then((data) => {
          seats = [];
          data.seats[0].rows.map((d, i) => {
            seats.push(d);
          })
          resolve(seats);
        })
    });
  };

  seatsRequest = (val, id) => {
    // const data = {name:val, id:"s_71"};
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.1.7:3001/getTotalSeats/${val}/${id}`)
        .then((response) => response.json())
        .then((data) => {
          seats = [];
          data.seats[0].rows.map((d, i) => {
            seats.push(d);
          })
          resolve(seats);
        })
    });
  };

  getData = (val) => {
    this.xhrRequest(val).then((sections) => {
      this.setState({ sections });
    });
  };

  xhrRequest = (val) => {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        method: "GET"
      };
      fetch(`http://192.168.1.7:3001/getSection/${val}`)
        .then((response) => response.json())
        .then((data) => {
          paths = [];
          data.sect.sections.map((d, i) => {
            paths.push(d);
          })
          resolve(paths);
        })
    });
  };

  zoomable = React.createRef();

  reset = () => {
    this.zoomable.current.reset();
  };
  zoomIn = () => {
    this.zoomable.current.zoomBy(1.5, 10, 10);
  };

  render() {
    const { width, height } = Dimensions.get('window');
    const { toggle } = this.state;
    return (
      <>
        {/* <View style={{  }}
        // {...this._panResponder.panHandlers}
        > */}
        <View style={styles.container}>
          <ZoomableSvg
            ref={this.zoomable}
            align="mid"
            width={width}
            height={height}
            vbWidth={7680}
            vbHeight={10240}
            viewBoxSize={10000}
            svgRoot={({ transform }) => (
              <Svg
                width={width}
                height={height}
                preserveAspectRatio="xMinYMin meet"
              // viewBox={"0 0 5000 5000"}
              // x="0px" y="0px" width="1024px" height="768px"
              // viewBox="0 0 1000 1000"
              // preserveAspectRatio="xMinYMin meet"
              >
                <G transform={transform}>
                  {stadiumImages.map(d => {
                    if (d.title == this.state.name) {
                      return (
                        <Image
                          x="0"
                          y="0"
                          width="10200"
                          height="7700"
                          // preserveAspectRatio="xMidYMid slice"
                          opacity="1"
                          href={d.file}
                        />
                      )
                    }
                  })}

                  {this.state.sections.map((item, key) => (
                    <Path
                      key={key}
                      d={item.path.d}
                      fill={(this.state.sectionSelected == item.sectionId) ? "yellow" : "none"}
                      stroke={(this.state.sectionSelected == item.sectionId) ? "blue" : "green"}
                      strokeWidth={(this.state.sectionSelected == item.sectionId) ? "5" : "10"}
                      onPress={() => {
                        this.setState({
                          sectionSelected: item.sectionId
                        })
                        this.getSeats(this.state.name.replace('-', ' '), item.sectionId);
                        // console.log("section = " + `${item.sectionId}`)
                      }}
                    />
                  )
                  )}
                  {seats.map((a, i) => {
                    seatsInRows = [];
                    for (let k = 0; k < a.seats.length; k++) {
                      seatsInRows.push(
                        <G>
                          <Circle
                            key={a.seats[k].id + "000" + k}
                            cx={a.seats[k].cx}
                            cy={a.seats[k].cy}
                            r={(this.state.seatSelected == a["data-row-name"] + a.seats[k]["data-seat-name"]) ? 6 : 4}
                            stroke={(this.state.seatSelected == a["data-row-name"] + a.seats[k]["data-seat-name"]) ? "black" : "white"}
                            fill={(this.state.seatSelected == a["data-row-name"] + a.seats[k]["data-seat-name"]) ? "white" : "red"}
                            onPress={() => {
                              this.setState({
                                seatSelected: a["data-row-name"] + a.seats[k]["data-seat-name"]
                              })
                            }} />
                          <TSpan
                            fill="white"
                            fontSize="4"
                            dx={a.seats[k].cx}
                            dy={a.seats[k].cy}
                            textAnchor="middle">{a.seats[k]["data-seat-name"]}</TSpan>
                        </G>
                      );
                    }
                    return seatsInRows;
                  })}
                </G>
              </Svg>
            )}
          />
        </View>
        <View style={{ flex: 0.5, flexDirection: 'row' }}>
          <Button
            title="Reset"
            onPress={this.reset}
          />
          <Button
            title="Zoom In"
            onPress={this.zoomIn}
          />
          <SelectDropdown
            data={stadiumsList}
            onSelect={(selectedItem, index) => {
              this.setState({
                name: selectedItem.replace(/\s+/g, '-'),
              })
              this.getData(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem
            }}
            rowTextForSelection={(item, index) => {
              return item
            }}
          />
          <Text>{(this.state.name) ? this.state.name : 'Loading....'}</Text>
        </View>
      </>
    );
  }
}
export default TextInANest;


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    flex: 1.5, flexDirection: 'column',
    width: "100%", height: "100%", backgroundColor: 'powderblue'
  },
});