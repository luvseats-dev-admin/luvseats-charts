const Pool = require("pg").Pool;
const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "seating",
  password: "72342",
  port: 5432,
});

const getRecords = () => {
  return new Promise(function (resolve, reject) {
    pool.query("SELECT test FROM stadiums", (error, results) => {
      if (error) {
        reject(error)
      }
      resolve(results.rows);
    });
  });
};

const getStadiums = () => {
  return new Promise(function (resolve, reject) {
    pool.query("SELECT name FROM stadiums order by name", (error, results) => {
      if (error) {
        console.log(error);
        reject(error)
      }
      console.log(results.rows);
      resolve(results.rows);
    });
  });
};

const getSections = (name) => {
  return new Promise(function (resolve, reject) {
    pool.query("SELECT sect FROM stadiums WHERE name = $1", [name], (error, results) => {
      if (error) {
        console.log(error);
        reject(error)
      }
      console.log(results.rows[0]);
      resolve(results.rows[0]);
    });
  });
};

const getSeats = (name) => {
  return new Promise(function (resolve, reject) {
    pool.query("SELECT ( SELECT json_agg(element) FROM json_array_elements(seats -> 'sections') element WHERE  element ->> 'sectionId' = 's_132') AS seats FROM stadiums where name = $1", [name], (error, results) => {
      if (error) {
        console.log(error);
        reject(error)
      }
      console.log(results.rows[0]);
      resolve(results.rows[0]);
    });
  });
};

const getTotalSeats = (id, name) => {
  return new Promise(function (resolve, reject) {
    pool.query("SELECT ( SELECT json_agg(element) FROM json_array_elements(seats -> 'sections') element WHERE  element ->> 'sectionId' = $1) AS seats FROM stadiums where name = $2", [id, name], (error, results) => {
      if (error) {
        console.log(error);
        reject(error)
      }
      console.log(results.rows[0]);
      resolve(results.rows[0]);
    });
  });
};

const createRecord = (body) => {
  return new Promise(function (resolve, reject) {
    const { name, email } = body;
    pool.query(
      "INSERT INTO demo(name, email) VALUES($1, $2)",
      [name, email],
      (err, res) => { }
    );
  });
};
const deleteRecord = () => {
  return new Promise(function (resolve, reject) {
    const id = request.params.id;
    pool.query("DELETE FROM demo WHERE name = '$1'", [id], (error, results) => {
      if (error) {
        reject(error);
      }
      resolve(`Record deleted with ID`);
    });
  });
};

module.exports = {
  getRecords,
  getStadiums,
  getSections,
  getSeats,
  getTotalSeats,
  createRecord,
  deleteRecord,
};
