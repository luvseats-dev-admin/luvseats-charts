const fs = require("fs");
const _ = require("lodash");
const SVG = require("svg-parser");

const svgImg = fs.readFileSync(`${process.cwd()}/input/Seats.svg`);
const svgMap = SVG.parse(String(svgImg));

const pathes = [];
let sections = [];
const labels = [];

function replacePath(item) {
  const tagId = _.get(item, "properties.data-section-id");
  const properties = _.get(item, "properties");
  pathes.push({
    ...properties,
  });
}

function replaceCircleToSeat(circle, sectionName) {
  const properties = _.get(circle, "properties");
  const id = _.get(properties, "id");
  const name = _.get(properties, "data-seat-name");
  const cx = _.get(properties, "cx");
  const cy = _.get(properties, "cy");

  return {
    id,
    "data-seat-name": `${name}`,
    cx,
    cy,
    sectionName: `${sectionName}`,
  };
}

function getSeats(row, sectionName) {
  const children = _.get(row, "children");
  const properties = _.get(row, "properties");
  const name = _.get(properties, "data-row-name");
  const seats = children.map((seat) => replaceCircleToSeat(seat, sectionName));
  return { "data-row-name": `${name}`, seats };
}

function getRows(section) {
  const children = _.get(section, "children");
  const properties = _.get(section, "properties");
  const sectionName = _.get(properties, "data-section-name");
  const rows = children.map((row) => getSeats(row, sectionName));
  const name = _.get(properties, "data-row-name");
  const id = _.get(properties, "data-section-id");
  return { ...properties, "data-section-id": id, "data-row-name": name, rows };
}

function getSections(children) {
  if (_.isArray(children)) {
    sections = _.groupBy(children.map(getRows), "data-section-id");
    const secs = [];
    _.forIn(sections, (value, key) => {
      // console.log(value);
      const rows = [];
      value.forEach((item) => {
        rows.push(...item.rows);
      });
      // console.log(`key ${key} # rows ${rows.length}`);
      const validate = _.unionBy(value, "data-section-name").map(
        (item) => `${item["data-section-name"]}`
      );
      const p = _.find(pathes, { "data-section-id": key });
      const name = _.get(p, "data-section-name");
      const d = _.get(p, "d");

      secs.push({
        sectionId: key,
        details: { name: `${name}`, validate },
        rows,
        path: { d },
      });
    });
    sections = secs;
  }
}

function getPaths(children) {
  if (_.isArray(children)) {
    children.map(replacePath);
  }
}

function transformTspan(tSpan) {
  const text = _.get(tSpan, "children[0].value");
  const properties = _.get(tSpan, "properties");
  const { x, y, dy } = properties;
  return { text: text ? text.trim() : "", x: `${x}`, y: `${y}`, dy: `${dy}` };
}

function getLabel(label) {
  const children = _.get(label, "children");
  const properties = _.get(label, "properties");
  const sectionId = _.get(properties, "data-label-id");
  const transform = _.get(properties, "transform");
  const fontSize = _.get(properties, "font-size");
  const tspans = children.map(transformTspan);
  labels.push({
    transform,
    fontSize: `${fontSize}`,
    sectionId,
    tspans,
  });
}

function getLabels(lebals) {
  if (_.isArray(lebals)) {
    lebals.map(getLabel);
  }
}

function joinSectionsAndLabels() {
  _.forEach(sections, (sec) => {
    const sectionId = _.get(sec, "sectionId");
    const label = _.find(labels, { sectionId: sectionId });
    sec.label = label;
  });
}

function recursiveRemap(source) {
  const children = _.get(source, "children");
  const tagName = _.get(source, "tagName");
  const tagId = _.get(source, "properties.data-section-id");
  const properties = _.get(source, "properties");
  const className = _.get(properties, "class");
  if (tagName === "g" && className === "labels") {
    getLabels(children);
    console.log(className);
  } else if (tagName === "g" && className === "polygons") {
    console.log(className);
    getPaths(children);
  } else if (tagName === "g" && className === "seats") {
    console.log(className);
    getSections(children);
  } else if (_.isArray(children)) {
    children.map((item) => recursiveRemap(item, tagId));
  }
}

const main = () => {
  const now = new Date();
  const stadiumName = _.get(process.argv, "[2]", now.getTime());

  const output = `${process.cwd()}/output`;

  if (!fs.existsSync(output)) {
    fs.mkdirSync(output);
  }

  const dir = `${output}/${stadiumName}`;
  try {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
      recursiveRemap(svgMap);
      joinSectionsAndLabels();

      fs.writeFileSync(
        `${dir}/seats.json`,
        JSON.stringify({ sections }, null, 4)
      );
      console.log(`Creating ${stadiumName} completed!!`);
    } else {
      console.log("Existing stadium name Choose another name");
    }
  } catch (error) {
    console.log(error);
  }
};

main();
