import MainStage from "./mapping"
import './App.css';
import './seats.scss';

function App() {
  return (
    <MainStage
    />
  );
}

export default App;
