import React, { useRef, useEffect, useState } from "react";
import { UncontrolledReactSVGPanZoom } from "react-svg-pan-zoom";
import { useWindowSize } from "@react-hook/window-size";
import background from "./background.png";
import stand from "./stands/standview.jpg";
import { Tooltip } from "react-svg-tooltip";
import svgPathBbox from "svg-path-bbox"
import Popup from 'reactjs-popup';
// import $ from "jquery";

const useFetch = (url) => {
  //load local json map
  const [data, setData] = useState(null);
  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        setData(data.seats.sections);
      });
  }, [url]);
  return data;
};

const MainStage = () => {
  //call local json load
  const jsonData = useFetch("./seats-data.json");
  const [selectedSeats, setselectedSeats] = useState([]);
  // const selectedSeats = [];
  const subSections = [];
  const rowsInSubSections = [];
  const seatsInRows = [];

  let Viewer = useRef(null);

  useEffect((a) => {
    if(jsonData)
      Viewer.fitSelection(0, 0, 10500, 7700);
  }, [jsonData]);

  const [width, height] = useWindowSize({
    initialWidth: 100,
    initialHeight: 100,
  });

  // show loading... text while SVG loads
  if (jsonData === null) {
    return "Loading...";
  }

  const showHideMenu = (a) => {
    (a.target.classList.value.indexOf("change") !== -1) ?
      document.getElementById("mySidenav").style.width = "350px" :
      document.getElementById("mySidenav").style.width = "0"
  }
  // function to handle seat click
  const handleSeatClick = (a) => {
    a.target.classList.remove("hovered");
    if (a.target.classList.value.indexOf("active") !== -1) {
      a.target.classList.remove("active");
      const index = selectedSeats.indexOf(a.target.id);
      if (index > -1) {
        selectedSeats.splice(index, 1);
        setselectedSeats([...selectedSeats])
          // document.getElementById("mySidenav").style.width = "0";

      }
    }
    else {
      setselectedSeats(arr => [...arr, `${a.target.id}`])
      a.target.classList.add("active");
      document.getElementById("mySidenav").style.width = "350px";
    }
  };

  // function to handle seat hover
  const handleHover = (a) => {
    a.target.classList.value.indexOf("active") !== -1
      ? a.target.classList.remove("hovered")
      : a.target.classList.add("hovered");
  };

  // function to handle seat hover out
  const closeNav = () => {
    document.getElementById("mySidenav").style.width = "0";
  };

  // function to handle seat hover out
  const handleMouseLeave = (a) => {
    a.target.classList.remove("hovered");
  };

  const zoomToSection = (id) => {
    const bbox = svgPathBbox(id);
    console.log(bbox);
    // Viewer.fitSelection(bbox[0],bbox[1],bbox[2],bbox[3])
    Viewer.fitSelection(0, 0, bbox[0], bbox[1]);
  }

  const circleRef = React.createRef();
  const popupRef = React.createRef();

  return (
    <div>
      <div id="mySidenav" className="sidenav"><h1>Your Tickets</h1>
        <div id="container" class="change" onClick={showHideMenu}>
          <div class="bar1"></div>
          <div class="bar2"></div>
          <div class="bar3"></div>
        </div>
        <div>
          <div class="row">
            <div class="column">SEC</div>
            <div class="column">ROW</div>
            <div class="column">SEAT</div>
          </div>
          <button class="ticket-bag__remove-item-btn" data-bdd="ticket-bag-remove-btn">
            <span class="ada-text">Remove
            </span>
          </button>
          {selectedSeats.map(e => {
            let vals = e.split("-");
            // console.log(vals);
            return (<div class="row">
              <div class="column_value">{vals[0]}</div>
              <div class="column_value">{vals[1]}</div>
              <div class="column_value">{vals[2]}</div>
            </div>)
          }
          )}
        </div>
      </div>
      <div>
        <div data-bdd="legend" class="legend legend--with-items edp__ism-view-filter-button--opf-message">
          <button class="legend__display-toggle unbutton">
            <span>
              Legend
            </span>
            <svg class="legend__icon--chevron" role="img" focusable="false">
              <use xlinkHref="/edp/v1/static/ccp/img/svg/edp-sprite.svg#icon-chevron"></use>
            </svg>
          </button>
          <ul class="legend__items">
            <li class="legend__item">
              <div class="legend__item-info">
                <div class="legend__icon--primary">
                  <svg viewBox="0 0 100 100">
                    <g>
                      <circle cx="50" cy="50" r="40" fill="none" stroke="red" strokeWidth={15}>
                      </circle>
                    </g>
                  </svg>
                </div>
                <span>Available</span>
              </div>
            </li>
            <li class="legend__item" data-bdd="legend-item-resale">
              <div class="legend__item-info">
                <div class="legend__icon--resale">
                  <svg viewBox="0 0 100 100">
                    <g>
                      <circle cx="50" cy="50" r="40" fill="blue" stroke="black" strokeWidth={15}>
                      </circle>
                    </g>
                  </svg>
                </div>
                <span>Selected
                </span>
              </div>
            </li>
            <li class="legend__item">
              <div class="legend__item-info">
                <div class="legend__icon--unavailable">
                  <svg viewBox="0 0 100 100">
                    <g>
                      <circle cx="50" cy="50" r="50">
                      </circle>
                    </g>
                  </svg>
                </div>
                <span>
                  Unavailable
                </span>
              </div>
            </li>
            <li class="legend__item">
              <div class="legend__item-info">
                <div>
                  <svg viewBox="0 0 100 100">
                    <g>
                      <circle cx="50" cy="50" r="40" fill="yellow" stroke="blue" strokeWidth={15}>
                      </circle>
                    </g>
                  </svg>
                </div>
                <span>
                  Highlighted
                </span>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <UncontrolledReactSVGPanZoom
        width={width}
        height={height}
        background={"#ffffff"}
        ref={(view) => (Viewer = view)}
        defaultTool={"pan"}
        toolbarProps={{ position: "left" }}
      >
        <svg viewBox="0 0 10500 7700">
          <defs>
            <pattern
              id="img1"
              patternUnits="userSpaceOnUse"
              width={10500}
              height={7500}
            >
              <image
                xlinkHref={background}
                x="0"
                y="0"
                width={10250}
                height={7650}
              />
            </pattern>
          </defs>
          <rect fill="url(#img1)" x="0" y="0" width="9000" height="7500" />
          <text dx={9000} dy={200} fill="Blue" textAnchor="middle" fontSize="200">
            Las Vegas Ball Park
          </text>
          {jsonData.map((d, i) => {
            return (
              <g>
                <a
                  href="javascript:void(0)"
                  onClick={(i) => { }}
                >
                  <Popup
                    ref={popupRef}
                    closeOnDocumentClick={true}
                    position="left center"
                    trigger={
                      <path
                        ref={popupRef}
                        id={d.rows[0].seats[0].sectionName}
                        key={i}
                        d={d.path.d}
                        stroke="Green"
                        strokeWidth="20"
                        fill="white"
                      />
                    }
                  >
                    <div><h1>{d.rows[0].seats[0].sectionName}</h1>
                      <h3>View from Stand</h3>
                      <button onClick={() => zoomToSection(`${d.path.d}`)}>Jump To Section</button><br />
                      <img height={250} width={450} src={stand} /></div>
                  </Popup>
                </a>
              </g>
            );
          })}
          {jsonData.map((d, i) => {
            subSections.push(d);
          })}
          {subSections.map((d, i) => {
            for (let j = 0; j < d.rows.length; j++) {
              rowsInSubSections.push(d.rows[j]);
            }
          })}
          {rowsInSubSections.map((a, i) => {
            const obj = { 'rowname': a.datarowname, 'seats': a.seats };
            seatsInRows.push(obj);
          })}
          {seatsInRows.map((a, i) => {
            let seats = [];
            for (let k = 0; k < a.seats.length; k++) {
              seats.push(
                <g
                  id={a.rowname + "-" + a.seats[k].dataseatname}
                >
                  <circle
                    ref={circleRef}
                    key={a.seats[k].id + "000" + k}
                    cx={a.seats[k].cx}
                    cy={a.seats[k].cy}
                    r={10}
                    id={a.seats[k].sectionName + "-" + a.rowname + "-" + a.seats[k].dataseatname}
                    className="circ"
                    data-checked={false}
                    onClick={handleSeatClick}
                    onMouseEnter={handleHover}
                    onMouseLeave={handleMouseLeave}
                  />
                  <text
                    dx={a.seats[k].cx}
                    dy={a.seats[k].cy}
                    fill="black"
                    stroke="black"
                    strokeWidth={0.5}
                    textAnchor="middle"
                    fontSize="6"
                  >{a.rowname + "-" + a.seats[k].dataseatname}
                  </text>
                  <Tooltip triggerRef={circleRef}>
                    <rect
                      x={2}
                      y={2}
                      width={a.seats[k].dataseatname.length > 1 ? 230 : 220}
                      height={60}
                      rx={0.5}
                      ry={0.5}
                    />
                    <text x={20} y={20} fontSize={20} fill="white">
                      Section Name: {a.seats[k].sectionName}
                    </text>
                    <text x={75} y={50} fontSize={20} fill="white">
                      Seat #: {a.rowname + "-" + a.seats[k].dataseatname}
                    </text>
                  </Tooltip>
                </g>
              );
            }
            return seats;
          })}
          {jsonData.map((d, i) => {
            return (
              <text fontSize="75">
                <textPath xlinkHref={`#${d.rows[0].seats[0].sectionName}`} startOffset="50">
                  {d.rows[0].seats[0].sectionName}</textPath>
              </text>
            );
          })}
          {/*jsonData.map((d, i) => {
          return (
              <text
                dx={d.rows[0].seats[0].cx}
                dy={d.rows[0].seats[0].cy}
                fill="White"
                alignment-baseline="middle"
                fontSize="150"
                stroke-width="15"
                stroke="#000"
                text-anchor="middle"
              >
                {d.rows[0].seats[0].sectionName}
              </text>
          );
        })*/}
        </svg>
      </UncontrolledReactSVGPanZoom>
    </div>
  );
};

export default MainStage;
