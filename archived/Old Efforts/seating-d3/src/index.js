import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './styles/seats.scss';
import './styles/buttons.scss';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);