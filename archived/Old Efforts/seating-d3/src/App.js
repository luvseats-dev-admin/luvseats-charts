import React, { Component } from 'react';
import * as d3 from 'd3';
import jsonData from "./seats-data.json"
import background from "./background.png";

let zoom, svg;
const subSections = [];
const rowsInSubSections = [];
const seatsInRows = [];
const height = "10500", width = "7700", x = 0, y = 0;

class App extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  componentDidMount() {
    var obj = JSON.parse(JSON.stringify(jsonData));

    zoom = d3.zoom()
      .on('zoom', function (event) {
        svg.selectAll('g')
          .attr('transform', event.transform);
        // svg.selectAll("text")
        //   .attr('transform', event.transform);
        // svg.selectAll("path")
        //   .attr('transform', event.transform);
        svg.selectAll("rect")
          .attr('transform', event.transform);
      });

    // var responsivefy = (svg) => {
    //   const container = d3.select(svg.node().parentNode),
    //     width = parseInt(svg.style('width'), 10),
    //     height = parseInt(svg.style('height'), 10),
    //     aspect = width / height;
    //   svg.attr('viewBox', `0 0 ${width} ${height}`)
    //     .attr('preserveAspectRatio', 'xMinYMid')
    //     .call(resize);
    //   d3.select(window).on(
    //     'resize.' + container.attr('id'),
    //     resize
    //   );
    //   function resize() {
    //     const w = parseInt(container.style('width'));
    //     svg.attr('width', w);
    //     svg.attr('height', Math.round(w / aspect));
    //   }
    // }
    var tooltip = d3.select("div.tooltip");

    function getBBox(t) {
      var xmin, xmax, ymin, ymax, p;
      t = t.replace(/[a-z].*/g, " ")
        .replace(/[\sA-Z]+/gi, " ").trim().split(" ");

      for (var h in t) {
        if (t[h].length > 1) {
          p = t[h].split(",");
          xmin = xmax = p[0]; ymin = ymax = p[1];
        }
      }
      for (var i in t) {
        p = t[i].split(",");
        if (!p[1]) { p[0] = xmin; p[1] = ymin; }
        xmin = Math.min(xmin, p[0]);
        xmax = Math.max(xmax, p[0]);
        ymin = Math.min(ymin, p[1]);
        ymax = Math.max(ymax, p[1]);
      } return [[xmin, ymax], [xmax, ymin]];
    }

    svg = d3.select(this.myRef.current).append("svg")
      // .call(responsivefy)
      .attr("viewBox", x + " " + y + " " + height + " " + width)
      .call(zoom);

    var defs = svg.append('defs')
    defs.append('pattern')
      .attr('id', 'img1')
      .attr("width", 10500)
      .attr("height", 7500)
      .attr("patternUnits", "userSpaceOnUse")
      .append('svg:image')
      .attr('xlink:href', background)
      .attr("width", 10250)
      .attr("height", 7650)
      .attr("x", 0)
      .attr("y", 0);

    svg.append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("height", 7500)
      .attr("width", 9000)
      .style("fill", "url(#img1)");

    var elem = svg.selectAll("g")
      .data(obj.seats.sections)

    var elemEnter = elem.enter()
      .append("g")

    elemEnter.append("path")
      .attr("id", (datapoint) => {
        return datapoint.sectionId + "-" + datapoint.rows[0].seats[0].sectionName;
      })
      .attr("d", (datapoint) => datapoint.path.d)
      .style("fill", "white")
      .style("cursor", "pointer")
      .style("stroke-width", "5")
      .style("stroke", "green")
      .on("mouseover", function (d, i) {
        d3.select(this)
          .style("fill", "#026cdf")
          .style("stroke-width", 40)
          .style("stroke", "red");
        return tooltip.style("hidden", false).html(d.target.id.split('-')[1]);
      })
      .on("mousemove", function (event, d) {
        try {
          tooltip.classed("hidden", false)
            .style("top", (event.pageY) + "px")
            .style("left", (event.pageX + 10) + "px")
            .html(d.target.id.split('-')[1]);
        }
        catch (e) { }
      })
      .on("mouseout", function (d, i) {
        d3.select(this)
          .style("fill", "white")
          .style("stroke-width", 5)
          .style("stroke", "green");
        tooltip.classed("hidden", true);
      })
      .on("click", function (a, b) {
        {
          obj.seats.sections.map((c, d) => {
            subSections.push(c);
          })
        }
        {
          subSections.map((e, f) => {
            for (let j = 0; j < e.rows.length; j++) {
              if (e.sectionId == a.target.id.split('-')[0])
                rowsInSubSections.push(e.rows[j]);
            }
          })
        }
        {
          rowsInSubSections.map((a, i) => {
            const obj = { 'rowname': a.datarowname, 'seats': a.seats };
            seatsInRows.push(obj);
          })
        }
        {
          seatsInRows.map((a, i) => {
            for (let k = 0; k < a.seats.length; k++) {
              elemEnter.append("circle")
                .attr("cx", a.seats[k].cx)
                .attr("cy", a.seats[k].cy)
                .attr("r", 8)
                .style("fill", "white")
                .style("cursor", "pointer")
                .style("stroke", "red")
                .style("stroke-width", "1.5")
              elemEnter.append("text")
                .attr("dx", a.seats[k].cx)
                .attr("dy", a.seats[k].cy)
                .style("fill", "none")
                .style("fill-opacity", 0.1)
                .style("stroke", "black")
                .style("stroke-width", "0.5")
                .style("font-size", "6")
                .style("text-anchor", "middle")
                .text(a.rowname + "-" + a.seats[k].dataseatname)
            }
          })
        }
      })

    var elemEnterTwo = elem.enter()
      .append("g")
      .append("text")
      .attr("x", function (d) {
        // var bbox = getMyCentroidTwo(d.path.d);
        // console.log(bbox[1][0]-bbox[0][0]);
        var centroid = getBBox(d.path.d);
        return (centroid[1][0] + centroid[0][0]) / 2;
      })
      .attr("y", function (d) {
        var centroid = getBBox(d.path.d);
        return (centroid[1][1] + centroid[0][1]) / 2;
      })
      .text(function (p) { return p.rows[0].seats[0].sectionName })
      .style("font-size", 90)
      .style("font-weight", "bold")
      .style("fill", "red")
      .attr('text-anchor', 'middle')

    function clicked(d) {
      console.log(d);
      //   var x, y, k;

      //   if (d && centered !== d) {
      //     var centroid = path.centroid(d);
      //     x = centroid[0];
      //     y = centroid[1];
      //     k = 4;
      //     centered = d;
      //   } else {
      //     x = width / 2;
      //     y = height / 2;
      //     k = 1;
      //     centered = null;
      //   }

      //   g.selectAll("path")
      //     .classed("active", centered && function (d) { return d === centered; });

      //   g.transition()
      //     .duration(750)
      //     .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
      //     .style("stroke-width", 1.5 / k + "px");
    }
  }

  render() {
    return (
      <>
        <div className="wrapper">
          <header className="header"><h2>SOME HEADER</h2></header>
          <article className="main">
            <div ref={this.myRef}>
            </div>
            <div className="tooltip"></div>
            <div id="legend">
              <div data-bdd="legend" className="legend legend--with-items edp__ism-view-filter-button--opf-message">
                <ul className="legend__items">
                  <li className="legend__item">
                    <div className="legend__item-info">
                      <div className="legend__icon--primary">
                        <svg viewBox="0 0 100 100">
                          <g>
                            <circle cx="50" cy="50" r="40" fill="none" stroke="red" strokeWidth={15}>
                            </circle>
                          </g>
                        </svg>
                      </div>
                      <span>Available</span>
                    </div>
                  </li>
                  <li className="legend__item" data-bdd="legend-item-resale">
                    <div className="legend__item-info">
                      <div className="legend__icon--resale">
                        <svg viewBox="0 0 100 100">
                          <g>
                            <circle cx="50" cy="50" r="40" fill="blue" stroke="black" strokeWidth={15}>
                            </circle>
                          </g>
                        </svg>
                      </div>
                      <span>Selected
                      </span>
                    </div>
                  </li>
                  <li className="legend__item">
                    <div className="legend__item-info">
                      <div className="legend__icon--unavailable">
                        <svg viewBox="0 0 100 100">
                          <g>
                            <circle cx="50" cy="50" r="50">
                            </circle>
                          </g>
                        </svg>
                      </div>
                      <span>
                        Unavailable
                      </span>
                    </div>
                  </li>
                  <li className="legend__item">
                    <div className="legend__item-info">
                      <div>
                        <svg viewBox="0 0 100 100">
                          <g>
                            <circle cx="50" cy="50" r="40" fill="yellow" stroke="blue" strokeWidth={15}>
                            </circle>
                          </g>
                        </svg>
                      </div>
                      <span>
                        Highlighted
                      </span>
                    </div>
                  </li>
                  <li className="legend__item">
                    <div className="legend__item-info">
                      <div>
                        <svg viewBox="0 0 100 100">
                          <g>
                            <rect height="100" width="100" fill="none" stroke="green" strokeWidth={15}>
                            </rect>
                          </g>
                        </svg>
                      </div>
                      <span>
                        Sections
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="icons">
              <div className="icon zoomin" onClick={zoomSvg}>
                <div className="tooltip">Zoom In</div>
                <span><i className="fa fa-plus"></i></span>
              </div>
              <div className="icon zoomout" onClick={zoomOutSvg}>
                <div className="tooltip">Zoom Out</div>
                <span><i className="fa fa-minus"></i></span>
              </div>
              <div className="icon reset" onClick={resetSvg}>
                <div className="tooltip">Reset</div>
                <span><i className="fa fa-undo"></i></span>
              </div>
              <div className="icon panleft" onClick={panLeftSvg}>
                <div className="tooltip">Pan Left</div>
                <span><i className="fa fa-arrow-left"></i></span>
              </div>
              <div className="icon panright" onClick={panRightSvg}>
                <div className="tooltip">Pan Right</div>
                <span><i className="fa fa-arrow-right"></i></span>
              </div>
              <div className="icon center" onClick={centerSvg}>
                <div className="tooltip">Center</div>
                <span><i className="fa fa-align-center"></i></span>
              </div>
            </div>
          </article>
          <aside className="aside aside-2"><h1>Ticketing Menu Goes Here</h1></aside>
          <footer className="footer">
            ©Copyright Ticketing Application</footer>
        </div>
      </>
    );
  }
}

function zoomSvg() {
  zoom.scaleBy(svg.transition().duration(500), 1.5);
}

function zoomOutSvg() {
  zoom.scaleBy(svg.transition().duration(500), 0.5);
}

function resetSvg() {
  d3.select('svg')
    .transition()
    .call(zoom.scaleTo, 1);
}

function panRightSvg() {
  d3.select('svg')
    .transition()
    .call(zoom.translateBy, 100, 0);
}

function panLeftSvg() {
  d3.select('svg')
    .transition()
    .call(zoom.translateBy, -100, 0);
}

function centerSvg() {
  d3.select('svg')
    .transition()
    .call(zoom.translateTo, 0.5 * height, 0.5 * width);
}

export default App;