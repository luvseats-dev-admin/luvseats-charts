/* eslint-disable react-native/no-inline-styles */

// import React, { Component } from 'react';
// import { Text, View, StyleSheet } from 'react-native';
// import { Circle, G, Path, Svg, Image, Rect, Defs, Pattern, Ellipse } from 'react-native-svg';
// import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
// import jsonData from "./seats-data.json";
// import background from "./background.png";
// import * as d3 from 'd3';

// var obj = JSON.parse(JSON.stringify(jsonData));

// const SVGHeight = 10500;
// const SVGWidth = 7700;

// class TextInANest extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       titleText: "Bird's Nest",
//       bodyText: "This is not really a bird nest."
//     };
//   }

//   onPressTitle = () => {
//     this.setState({ titleText: "Bird's Nest [pressed]" });
//   };

//   render() {
//     return (
//       <>
//         <SvgPanZoom
//           canvasHeight={500}
//           canvasWidth={500}
//           minScale={0.1}
//           initialZoom={0.7}
//           maxScale={10}
//           onZoom={(zoom) => { console.log('onZoom:' + zoom) }}
//           canvasStyle={{ backgroundColor: 'white' }}
//           viewStyle={{ backgroundColor: 'white' }}
//         >
//           <Svg viewBox={"0 0 " + SVGHeight + " " + SVGWidth}>
//             <Defs>
//               <Pattern
//                 id="img1"
//                 patternUnits="userSpaceOnUse"
//                 width={7500}
//                 height={10500}
//               >
//                 <Image
//                   xlinkHref={require('./background.png')}
//                   x="0"
//                   y="0"
//                   width={7650}
//                   height={10250}
//                 />
//               </Pattern>
//             </Defs>
//             <Rect fill="none" stroke="blue" x="0" y="0" width="9000" height="7500" />
//             <SvgPanZoomElement
//               x={0}
//               y={0}
//               onClick={() => { console.log('onClick!') }}
//               onClickCanceled={() => { console.log('onClickCanceled!') }}
//               onClickRelease={() => { console.log('onClickRelease!') }}
//               onDrag={() => { console.log('onDrag!') }}
//             >
//               {obj.seats.sections.map((d, i) => {
//                 return (
//                   <Path d={d.path.d} key={i} id={d.rows[0].seats[0].sectionName} stroke="green" strokeWidth="50" fill="white"></Path>
//                 );
//               })}
//             </SvgPanZoomElement>
//           </Svg>

//         </SvgPanZoom>
//         <View style={{ flex: 1, flexDirection: 'row' }}>
//           <View style={{
//             backgroundColor: 'blue',
//             flexGrow: 1,
//           }}>
//             <Text>Fluid</Text>
//           </View>
//           <View style={{
//             backgroundColor: 'red',
//             width: 100,
//           }}>
//             <Text>Fixed</Text>
//           </View>
//         </View>
//       </>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   baseText: {
//     fontFamily: "Cochin"
//   },
//   titleText: {
//     fontSize: 20,
//     fontWeight: "bold"
//   }
// });

// export default TextInANest;


import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Circle, G, Path, Svg, Image, Rect, Defs, Pattern, Ellipse } from 'react-native-svg';
import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
import jsonData from "./seats-data.json";

var obj = JSON.parse(JSON.stringify(jsonData));

const SVGHeight = 10500;
const SVGWidth = 7700;

class TextInANest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false
    };
  }

  toggle = () => {
    this.setState({ hover: !this.state.hover });
  };

  pressed = (e) => {
    console.log(this);
  };
  render() {
    return (
      <>
        <SvgPanZoom
          canvasHeight={500}
          canvasWidth={500}
          minScale={0.1}
          initialZoom={0.7}
          maxScale={10}
          onZoom={(zoom) => { console.log('onZoom:' + zoom) }}
          canvasStyle={{ backgroundColor: 'white' }}
          viewStyle={{ backgroundColor: 'white' }}
        >
          <Svg viewBox={"0 0 " + SVGHeight + " " + SVGWidth}>
            <Defs>
              <Pattern
                id="img1"
                patternUnits="userSpaceOnUse"
                width={7500}
                height={10500}
              >
                <Image
                  source={require('./assets/background.png')}
                  x="0"
                  y="0"
                  width="100%"
                  height="100%"
                />
              </Pattern>
              <Pattern
                id="Stripes"
                width={1 / 7 * 2}
                height="1"
                patternContentUnits="objectBoundingBox"
              >
                <Rect
                  x="0"
                  y="0"
                  height="1"
                  width={1 / 7}
                  fill="#ffffff"
                />
                <Rect
                  x={1 / 7}
                  y="0"
                  height="1"
                  width={1 / 7}
                  fill="#000000"
                />
              </Pattern>
            </Defs>
            <Rect fill="url(#Stripes)" stroke="blue" x="0" y="0" width="9000" height="7500" />
            <SvgPanZoomElement
              x={0}
              y={0}
              onClick={() => { console.log('onClick!') }}
              onClickCanceled={() => { console.log('onClickCanceled!') }}
              onClickRelease={() => { console.log('onClickRelease!') }}
              onDrag={() => { console.log('onDrag!') }}
            >
              {obj.seats.sections.map((d, i) => {
                return (
                  <Path d={d.path.d} key={i} id={d.rows[0].seats[0].sectionName} strokeWidth="50"
                    delayPressIn={0}
                    // onPress={this.pressed}
                    onPressIn={this.toggle}
                    onPressOut={this.toggle}
                    stroke={this.state.hover ? 'yellow' : 'green'}
                    fill={this.state.hover ? 'blue' : 'white'}
                    ></Path>
                );
              })}
            </SvgPanZoomElement>
          </Svg>

        </SvgPanZoom>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{
            backgroundColor: 'blue',
            flexGrow: 1,
          }}>
            <Image
              source={require("./assets/background.png")}
            ></Image>
          </View>
          <View style={{
            backgroundColor: 'red',
            width: 100,
          }}>
            <Text>Fixed</Text>
          </View>
        </View>
      </>
    );
  }
}

export default TextInANest;