import React, { useEffect, useRef, useState, useLayoutEffect } from "react";

import { INITIAL_VALUE, ReactSVGPanZoom, TOOL_NONE } from "react-svg-pan-zoom";
import { ReactSvgPanZoomLoader } from "react-svg-pan-zoom-loader";
import { useWindowSize } from "@react-hook/window-size";

const App = (args) => {
  const Viewer = useRef(null);
  const [tool, onChangeTool] = useState(TOOL_NONE);
  const [value, onChangeValue] = useState(INITIAL_VALUE);
  const [width, height] = useWindowSize({
    initialWidth: window.innerWidth,
    initialHeight: window.innerHeight,
  });

  useLayoutEffect(() => {
    Viewer.current.fitToViewer();
  }, []);
  const [records, setrecords] = useState(false);
  useEffect(() => {
    getRecords();
  }, []);
  function getRecords() {
    fetch("http://localhost:3001")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setrecords(data);
      });
  }
  function createRecord() {
    let name = prompt("Enter record name");
    let email = prompt("Enter record email");
    fetch("http://localhost:3001/records", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name, email }),
    })
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        alert(data);
        getRecords();
      });
  }
  function deleteRecord() {
    let id = prompt("Enter record id");
    fetch(`http://localhost:3001/records/${id}`, {
      method: "DELETE",
    })
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        alert(data);
        getRecords();
      });
  }

  return (
    <div className="fill-window">
      {records ? records : "There is no record data available"}
      <ReactSvgPanZoomLoader
        width={width}
        height={height}
        ref={Viewer}
        src="svg_data_trim_zoomed.svg"
        render={(content) => (
          <ReactSVGPanZoom
            width={width}
            height={height}
            ref={Viewer}
            value={value}
            onChangeValue={onChangeValue}
            tool={tool}
            onChangeTool={onChangeTool}
          >
            <svg width={width} height={height}>
              {content}
            </svg>
          </ReactSVGPanZoom>
        )}
      />
    </div>
  );
};
export default App;
