const express = require("express");
const app = express();
const port = 3001;
const cors = require("cors");
app.use(cors());

const record_db = require("./db_functions");

app.use(express.json());
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, Access-Control-Allow-Headers"
  );
  next();
});

app.get("/", (req, res) => {
  record_db
    .getRecords()
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      // res.status(500).send(error);
    });
});

app.get("/getStadiums", (req, res) => {
  record_db
    .getStadiums()
    .then((response) => {
      // console.log(response);
      res.status(200).send(response);
    })
    .catch((error) => {
      console.log(error);
      // res.status(500).send(error);
    });
});

app.get("/getSection/:name", (req, res) => {
  record_db
    .getSections(req.params.name)
    .then((response) => {
      // console.log(response);
      res.status(200).send(response);
    })
    .catch((error) => {
      console.log(error);
      // res.status(500).send(error);
    });
});

app.get("/getSeats/:name", (req, res) => {
  record_db
    .getSeats(req.params.name)
    .then((response) => {
      // console.log(response);
      res.status(200).send(response);
    })
    .catch((error) => {
      console.log(error);
      // res.status(500).send(error);
    });
});


app.get("/getTotalSeats/:name/:id", (req, res) => {
  record_db
    .getTotalSeats(req.params.id, req.params.name)
    .then((response) => {
      // console.log(response);
      res.status(200).send(response);
    })
    .catch((error) => {
      console.log(error);
      // res.status(500).send(error);
    });
});
// app.post("/records", (req, res) => {
//   record_db
//     .createrecord(req.body)
//     .then((response) => {
//       res.status(200).send(response);
//     })
//     .catch((error) => {
//       res.status(500).send(error);
//     });
// });

// app.delete("/records/:id", (req, res) => {
//   record_db
//     .deleterecord(req.params.id)
//     .then((response) => {
//       res.status(200).send(response);
//     })
//     .catch((error) => {
//       res.status(500).send(error);
//     });
// });
app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
